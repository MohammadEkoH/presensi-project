<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
$route['default_controller']            = 'auth';
$route['404_override']                  = '';
$route['translate_uri_dashes']          = FALSE;

// Profile
$route['profile']                       = 'Profile';

// Dashboard
$route['dashboard']                     = 'Admin';
$route['show-alat']                     = 'Absen/showAlat';

// Absen
$route['cronjob-siswa']                 = 'Absen/storeSiswa';
$route['cronjob-pegawai']               = 'Absen/storePegawai';
$route['update-timenow']                = 'Absen/storeTimeNow'; 
$route['store-siswa?(:any)']            = 'Absen/storeAbsensiSiswa';
$route['store-pegawai?(:any)']          = 'Absen/storeAbsensiPegawai';
$route['get-data-siswa']                = 'Absen/getDataUserSiswa';
$route['get-data-siswa/(:any)']         = 'Absen/getDataUserSiswa';
$route['get-data-pegawai']              = 'Absen/getDataUserPegawai';
$route['get-data-pegawai/(:any)']       = 'Absen/getDataUserPegawai';
$route['status-alat?(:any)']            = 'Absen/statusAlat';

// Settings
$route['settings']                      = 'Settings';
$route['settings/(:any)']               = 'Settings';
$route['s/profile']                     = 'Settings/viewProfile';
$route['s/profile?(:any)']              = 'Settings/viewProfile';
$route['s/alat']                        = 'Settings/viewAlat';
$route['s/alat?(:any)']                 = 'Settings/viewAlat';
$route['s/pegawai']                     = 'Settings/viewPegawai';
$route['s/pegawai?(:any)']              = 'Settings/viewPegawai';
$route['s/siswa']                       = 'Settings/viewSiswa';
$route['s/siswa?(:any)']                = 'Settings/viewSiswa';

// Jabatan
$route['s/jb/insert-now']               = 'Pegawai/addJabatan';
$route['s/jb/insert']                   = 'Pegawai/viewAddJabatan';
$route['s/jb/insert?(:any)']            = 'Pegawai/viewAddJabatan';
$route['s/jb/update-now']               = 'Pegawai/updateJabatan';
$route['s/jb/update?(:any)']            = 'Pegawai/viewUpdateJabatan';
$route['s/jb/delete?(:any)']            = 'Pegawai/deleteJabatan';

// Jadwal Pegawai
$route['s/jp/update-now']               = 'Pegawai/updateJadwal';
$route['s/jp/update?(:any)']            = 'Pegawai/viewUpdateJadwal';
$route['s/jp/delete?(:any)']            = 'Pegawai/deleteJadwal';

// TA Siswa 
$route['s/t/insert-now']                = 'Siswa/addTA';
$route['s/t/insert']                    = 'Siswa/viewAddTA';
$route['s/t/insert?(:any)']             = 'Siswa/viewAddTA';
$route['s/t/update?(:any)']             = 'Siswa/viewUpdateTA';
$route['s/t/delete?(:any)']             = 'Siswa/deleteTA';

// Jadwal Siswa 
$route['s/j/update-now']                = 'Siswa/updateJadwal';
$route['s/j/update?(:any)']             = 'Siswa/viewUpdateJadwal';
$route['s/j/delete?(:any)']             = 'Siswa/deleteJadwal';

// Kelas
$route['s/k/insert-now']                = 'Siswa/addKelas';
$route['s/k/insert']                    = 'Siswa/viewAddKelas';
$route['s/k/insert?(:any)']             = 'Siswa/viewAddKelas';
$route['s/k/update-now']                = 'Siswa/updateKelas';
$route['s/k/update?(:any)']             = 'Siswa/viewUpdateKelas';
$route['s/k/delete?(:any)']             = 'Siswa/deleteKelas';

// Alat 
$route['s/a/insert-now']                = 'Alat/addAlat';
$route['s/a/insert']                    = 'Alat/viewAddAlat';
$route['s/a/insert?(:any)']             = 'Alat/viewAddAlat';
$route['s/a/update-now']                = 'Alat/updateAlat';
$route['s/a/update?(:any)']             = 'Alat/viewUpdateAlat';
$route['s/a/delete?(:any)']             = 'Alat/deleteAlat';

//  Import Data
$route['a/p/import-data']               = 'Import/viewImportDataPegawai'; 
$route['a/p/import']                    = 'Import/importPegawai'; 
$route['a/s/import-data']               = 'Import/viewImportDataSiswa'; 
$route['a/s/import']                    = 'Import/importSiswa'; 

// Siswa
$route['a/s/data/(:any)']               = 'Siswa/viewDataSiswa'; 
$route['a/s/data?(:any)']               = 'Siswa/viewDataSiswa'; 
$route['a/s/insert-data']               = 'Siswa/viewAddDataSiswa'; 
$route['a/s/update-data?(:any)']        = 'Siswa/viewUpdateDataSiswa';   
$route['a/s/delete-data?(:any)']        = 'Siswa/deleteDataSiswa';   
$route['a/s/search-data?(:any)/(:any)'] = 'Siswa/searchDataSiswa'; 
$route['a/s/search-data?(:any)']        = 'Siswa/searchDataSiswa';  
$route['a/s/report-harian']             = 'Siswa/viewReportHarianSiswa';   
$route['a/s/report-harian-print?(:any)']= 'Siswa/viewReportHarianSiswaPrint';  
$route['a/s/report-bulanan']            = 'Siswa/viewReportBulananSiswa'; 
$route['a/s/report-bulanan-print?(:any)']= 'Siswa/viewReportBulananSiswaPrint';   
$route['edit-status-card-siswa?(:any)'] = 'Siswa/editStatusCardSiswa';

// Pegawai
$route['a/p/data/(:any)']               = 'Pegawai/viewDataPegawai'; 
$route['a/p/data?(:any)']               = 'Pegawai/viewDataPegawai'; 
$route['a/p/insert-data']               = 'Pegawai/viewAddDataPegawai'; 
$route['a/p/update-data']               = 'Pegawai/viewUpdateDataPegawai'; 
$route['a/p/delete-data?(:any)']        = 'Pegawai/deleteDataPegawai';  
$route['a/p/search-data?(:any)/(:any)'] = 'Pegawai/searchDataPegawai'; 
$route['a/p/search-data?(:any)']        = 'Pegawai/searchDataPegawai';  
$route['a/p/report-harian']             = 'Pegawai/viewReportHarianPegawai'; 
$route['a/p/report-harian-print?(:any)']= 'Pegawai/viewReportHarianPegawaiPrint';  
$route['a/p/report-bulanan']            = 'Pegawai/viewReportBulananPegawai';  
$route['a/p/report-bulanan-print?(:any)']= 'Pegawai/viewReportBulananPegawaiPrint';  
$route['edit-status-card-pegawai?(:any)'] = 'Pegawai/editStatusCardPegawai'; 