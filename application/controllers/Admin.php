<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller { 

	public function __construct()
    {
        parent::__construct();  
        if (!$this->ion_auth->logged_in()) {//cek login ga?
    		redirect('auth','refresh');
    	}else{
            if (!$this->ion_auth->in_group('admin')) {//cek admin ga?
                redirect('auth','refresh');
            }
        }
	}
	
	public function index()
	{      
        $data['DATA'] = $this->DBase->get_data('tb_alat', 'nama_alat', 'ASC')->result();  

        $this->template->admin('admin/vdashboard', $data); 
    }  
}
