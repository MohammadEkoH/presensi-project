<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller {
 
	public function __construct()
    {
        parent::__construct();  
        if (!$this->ion_auth->logged_in()) {//cek login ga?
    		redirect('auth','refresh');
    	}else{
            if (!$this->ion_auth->in_group('admin')) {//cek admin ga?
                redirect('auth','refresh');
            }
        }
	}
	
	public function index()
	{    
		$data['DATA_KELAS'] = $this->DBase->get_data('tb_kelas', 'kode_kelas', 'ASC')->result(); 

		if ($this->input->get('notif') == 'a1') { 
			$data['notif_berhasil'] = "	<div class='alert alert-success alert-dismissible'>
											<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
											<h4><i class='icon fa fa-check'></i> Successfully Adding New Data!</h4> 
										</div>";
		}else if ($this->input->get('notif') == 'a2') { 
			$data['notif_berhasil'] = "	<div class='alert alert-warning alert-dismissible'>
											<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
											<h4><i class='icon fa fa-check'></i> Successfully Updated Data!</h4> 
										</div>";
		}else if ($this->input->get('notif') == 'a3') { 
			$data['notif_berhasil'] = "	<div class='alert alert-danger alert-dismissible'>
											<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
											<h4><i class='icon fa fa-check'></i> Successfully Deleted Data!</h4> 
										</div>";
		}else {
			$data['notif_berhasil'] = "";
		}

		$this->template->admin('admin/settings/vsettings', $data);
	}
	
	// Profile
	public function viewProfile(){
		if ($this->input->get('notif') == 'a1') { 
			$data['notif_berhasil'] = "	<div class='alert alert-success alert-dismissible'>
											<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
											<h4><i class='icon fa fa-check'></i> Successfully Adding New Data!</h4> 
										</div>";
		}else if ($this->input->get('notif') == 'a2') { 
			$data['notif_berhasil'] = "	<div class='alert alert-warning alert-dismissible'>
											<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
											<h4><i class='icon fa fa-check'></i> Successfully Updated Data!</h4> 
										</div>";
		}else if ($this->input->get('notif') == 'a3') { 
			$data['notif_berhasil'] = "	<div class='alert alert-danger alert-dismissible'>
											<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
											<h4><i class='icon fa fa-check'></i> Successfully Deleted Data!</h4> 
										</div>";
		}else {
			$data['notif_berhasil'] = "";
		}

		$data['data_users'] = $this->DBase->get_data('users')->row();
		
		$this->template->admin('admin/settings/profile/vprofile', $data);
	}

	// Alat
	public function viewAlat(){
		$data['DATA_ALAT'] = $this->DBase->get_data('tb_alat', 'nama_alat', 'ASC')->result(); 
		if ($this->input->get('notif') == 'a1') { 
			$data['notif_berhasil'] = "	<div class='alert alert-success alert-dismissible'>
											<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
											<h4><i class='icon fa fa-check'></i> Successfully Adding New Data!</h4> 
										</div>";
		}else if ($this->input->get('notif') == 'a2') { 
			$data['notif_berhasil'] = "	<div class='alert alert-warning alert-dismissible'>
											<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
											<h4><i class='icon fa fa-check'></i> Successfully Updated Data!</h4> 
										</div>";
		}else if ($this->input->get('notif') == 'a3') { 
			$data['notif_berhasil'] = "	<div class='alert alert-danger alert-dismissible'>
											<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
											<h4><i class='icon fa fa-check'></i> Successfully Deleted Data!</h4> 
										</div>";
		}else {
			$data['notif_berhasil'] = "";
		}

		$this->template->admin('admin/settings/alat/valat', $data);
	}

	// Guru
	public function viewPegawai(){
		$data['DATA_JABATAN'] = $this->DBase->get_data('tb_jabatan', 'kode_pegawai', 'ASC')->result(); 
		$data['DATA_JADWAL'] = $this->DBase->get_data('tb_jadwal_pegawai', 'kode_jadwal', 'ASC')->result(); 

		if ($this->input->get('notif') == 'a1') { 
			$data['notif_berhasil'] = "	<div class='alert alert-success alert-dismissible'>
											<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
											<h4><i class='icon fa fa-check'></i> Successfully Adding New Data!</h4> 
										</div>";
		}else if ($this->input->get('notif') == 'a2') { 
			$data['notif_berhasil'] = "	<div class='alert alert-warning alert-dismissible'>
											<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
											<h4><i class='icon fa fa-check'></i> Successfully Updated Data!</h4> 
										</div>";
		}else if ($this->input->get('notif') == 'a3') { 
			$data['notif_berhasil'] = "	<div class='alert alert-danger alert-dismissible'>
											<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
											<h4><i class='icon fa fa-check'></i> Successfully Deleted Data!</h4> 
										</div>";
		}else {
			$data['notif_berhasil'] = "";
		}

		$this->template->admin('admin/settings/pegawai/vpegawai', $data);
	}

	// Siswa
	public function viewSiswa(){
		$data['DATA_KELAS'] = $this->DBase->get_data('tb_kelas', 'kode_kelas', 'ASC')->result(); 
		$data['DATA_JADWAL'] = $this->DBase->get_data('tb_jadwal_siswa', 'kode_jadwal', 'ASC')->result(); 
		$data['DATA_TA'] = $this->DBase->get_data('tb_ta', 'tahun_ajaran', 'ASC')->result(); 

		if ($this->input->get('notif') == 'a1') { 
			$data['notif_berhasil'] = "	<div class='alert alert-success alert-dismissible'>
											<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
											<h4><i class='icon fa fa-check'></i> Successfully Adding New Data!</h4> 
										</div>";
		}else if ($this->input->get('notif') == 'a2') { 
			$data['notif_berhasil'] = "	<div class='alert alert-warning alert-dismissible'>
											<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
											<h4><i class='icon fa fa-check'></i> Successfully Updated Data!</h4> 
										</div>";
		}else if ($this->input->get('notif') == 'a3') { 
			$data['notif_berhasil'] = "	<div class='alert alert-danger alert-dismissible'>
											<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
											<h4><i class='icon fa fa-check'></i> Successfully Deleted Data!</h4> 
										</div>";
		}else {
			$data['notif_berhasil'] = "";
		}

		$this->template->admin('admin/settings/siswa/vsiswa', $data);	
	}
}
