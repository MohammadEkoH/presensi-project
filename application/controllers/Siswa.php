<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa extends CI_Controller { 

	public function __construct()
    {
        parent::__construct();  
        if (!$this->ion_auth->logged_in()) {//cek login ga?
    		redirect('auth','refresh');
    	}else{
            if (!$this->ion_auth->in_group('admin')) {//cek admin ga?
                redirect('auth','refresh');
            }
        }
	} 

	public function updateAbsensiSiswa(){

		if($cek == 0 && $btn == 'update'){
			$data = array( 
				'jam_masuk' => date('H:i:s'), 
				'keterlambatan' => 0,
				'status' => $status
			);

			$this->DBase->update_data('tb_absensi_siswa', $where_id, $data);
		}
	}

	public function editStatusCardSiswa()
    {
        $id_siswa = $this->input->get('id_siswa');
        $where_id = array('id_siswa' => $id_siswa);
        $status = $this->input->get('status');
        
        if ($status == 1) {
            $data = array('status_smart_card' => 0);
        }else{
            $data = array('status_smart_card' => 1);
        }

        $this->DBase->update_data('tb_siswa', $where_id, $data);
		
		redirect('a/s/data');
    }

	public function viewDataSiswa()
	{   
		$data['data_ta'] = $this->DBase->get_data('tb_ta', 'tahun_ajaran', 'DESC')->result();
		$data['data_kelas'] = $this->DBase->get_data('tb_kelas', 'kode_kelas', 'DESC')->result(); 
		$data['data_jadwal'] = $this->DBase->get_data('tb_jadwal_siswa', 'kode_jadwal', 'DESC')->result(); 
		$data['count'] = $this->DBase->get_data('tb_siswa')->num_rows();
		
		//pagination settings
		$config['base_url'] = site_url('a/s/data');
		$config['total_rows'] = $data['count'];
		$config['per_page'] = "10";
		$config["uri_segment"] = 4;
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = floor($choice);
  
		//config for bootstrap pagination class integration
		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination pagination-sm no-margin pull-right">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';
  
		$this->pagination->initialize($config);
  
		$data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
  
		//call the model function to get the department data
		$data['data_siswa'] = $this->DBase->get_data_siswa($config['per_page'], $data['page']);
  
		$data['pagination'] = $this->pagination->create_links();

		if ($this->input->get('notif') == 1) { 
			$data['notif_berhasil'] = "	<div class='alert alert-success alert-dismissible'>
											<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
											<h4><i class='icon fa fa-check'></i> Successfully Adding New Student Data!</h4> 
										</div>";
		}else if ($this->input->get('notif') == 2) { 
			$data['notif_berhasil'] = "	<div class='alert alert-warning alert-dismissible'>
											<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
											<h4><i class='icon fa fa-check'></i> Successfully Update Data!</h4> 
										</div>";
		}else if ($this->input->get('notif') == 3) { 
			$data['notif_berhasil'] = "	<div class='alert alert-danger alert-dismissible'>
											<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
											<h4><i class='icon fa fa-check'></i> Successfully Delete Data!</h4> 
										</div>";
		}else{
			$data['notif_berhasil'] = "";
		} 

		$data['notif_limit'] = "";
		
		$this->template->admin('admin/siswa/vdataSiswa', $data);
	} 

	public function searchDataSiswa()
	{ 
		$nama = $this->input->post('nama');
		$kelas = $this->input->post('kelas');
		$ta = $this->input->post('ta'); 
		
        $data['uid'] = 1;
		$data['data_ta'] = $this->DBase->get_data('tb_ta', 'tahun_ajaran', 'DESC')->result();
		$data['data_kelas'] = $this->DBase->get_data('tb_kelas', 'kode_kelas', 'DESC')->result();
		$data['count'] = $this->DBase->count_data_siswa($kelas, $ta)->num_rows();
  
		$limit = 10; 
		
		$start = 0;
  
		//call the model function to get the department data
		$data['data_siswa'] = $this->DBase->search_data_siswa($nama, $kelas, $ta, $limit, $start);
	 
		$data['pagination'] = ""; 
		$data['notif_berhasil'] = ""; 

		if ($data['count'] == 0) {
			$data['notif_limit'] = "* Data Tidak ditemukan ";
		}else{
			$data['notif_limit'] = "* Pencarian dibatasi ". $limit;
		}
 
		$this->template->admin('admin/siswa/vdataSiswa', $data);
	} 

	public function viewAddDataSiswa()
	{
		$data['data_jadwal'] = $this->DBase->get_data('tb_jadwal_siswa')->result();
		$data['data_ta'] = $this->DBase->get_data('tb_ta')->result();
		$data['data_kelas'] = $this->DBase->get_data('tb_kelas')->result();
		if ($this->input->get('notif') == 1) { 
			$data['notif_gagal'] = "Please fill in the form";
		}else{
			$data['notif_gagal'] = "";
		}
 
		$this->template->admin('admin/siswa/vadd_data_siswa', $data);
	}  
	
	public function addDataSiswa()
	{
		$query = $this->DBase->get_id('tb_siswa', 'id_siswa');
		$id = $query->row();
		$id = $id->id_siswa;

		$id_siswa = substr($id, 7, 8);
		if ($id_siswa == 0) {
			$id_siswa = 1;
		}else {
			$id_siswa++;
		}

		$this->form_validation->set_rules('no_induk', 'No. Induk', 'required');
		$this->form_validation->set_rules('nisn', 'NISN', 'required');
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('jenkel', 'Jenis Kelamin', 'required');
		$this->form_validation->set_rules('tempat', 'Tempat', 'required');
		$this->form_validation->set_rules('ttl', 'Tanggal Lahir', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');
		$this->form_validation->set_rules('no_hp', 'No. HP', 'required'); 
		$this->form_validation->set_rules('kelas', 'Jurusan', 'required');
		$this->form_validation->set_rules('ta', 'Tahun Ajatran', 'required');

		$id			= $this->kode->kode_ID(random_string('alnum', 7), $id_siswa, $query); 
		$no_induk 	= $this->input->post('no_induk');
		$nisn 		= $this->input->post('nisn');
		$nama 		= $this->input->post('nama');
		$jenkel 	= $this->input->post('jenkel');
		$tempat		= $this->input->post('tempat');
		$ttl 		= $this->input->post('ttl');
		$alamat 	= $this->input->post('alamat');
		$no_hp 		= $this->input->post('no_hp'); 
		$kelas 		= $this->input->post('kelas');
		$ta 		= $this->input->post('ta'); 

		$data = array(
			'id_siswa'			=> $id,
			'no_induk_siswa' 	=> $no_induk,
			'nisn_siswa' 		=> $nisn,
			'nama_siswa' 		=> $nama,
			'jenkel_siswa' 		=> $jenkel,
			'tempat_siswa' 		=> $tempat,
			'tgl_lahir_siswa' 	=> $ttl,
			'alamat_siswa' 		=> $alamat,
			'no_hp_siswa' 		=> $no_hp, 
			'kode_kelas' 		=> $kelas,
			'tahun_ajaran'		=> $ta
		); 

		echo $no_induk; echo ", ";
		echo $nisn; 

		if ($this->form_validation->run() == FALSE)
		{   
			redirect('a/s/insert-data?notif=1', $data);
		}
		else
		{  
			$this->DBase->insert_data('tb_siswa', $data); 
			 
			redirect('a/s/data?notif=1');
		}  
    }  
    
    public function viewUpdateDataSiswa()
	{
        $id = $this->input->get('p');
        $where = array('id_siswa' => $id);

		$data['data_jadwal'] = $this->DBase->get_data('tb_jadwal_siswa')->result();
        $data['data_ta'] = $this->DBase->get_data('tb_ta')->result();
		$data['data_kelas'] = $this->DBase->get_data('tb_kelas')->result();
        $data['DATA'] = $this->DBase->get_where_data('tb_siswa', $where)->row();  

		if ($this->input->get('notif') == 2) { 
			$data['notif_gagal'] = "Please fill in the form";
		}else{
			$data['notif_gagal'] = "";
		}

		$this->template->admin('admin/siswa/vupdate_data_siswa', $data);
	}  

	public function updateDataSiswa()
	{
		$this->form_validation->set_rules('id', 'ID', 'required');
		$this->form_validation->set_rules('no_induk', 'No. Induk', 'required');
		$this->form_validation->set_rules('nisn', 'NISN', 'required');
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('jenkel', 'Jenis Kelamin', 'required');
		$this->form_validation->set_rules('tempat', 'Tempat', 'required');
		$this->form_validation->set_rules('ttl', 'Tanggal Lahir', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');
		$this->form_validation->set_rules('no_hp', 'No. HP', 'required'); 
		$this->form_validation->set_rules('kelas', 'Jurusan', 'required');
		$this->form_validation->set_rules('ta', 'Tahun Ajatran', 'required');

		$id 		= $this->input->post('id');
		$no_induk 	= $this->input->post('no_induk');
		$nisn 		= $this->input->post('nisn');
		$nama 		= $this->input->post('nama');
		$jenkel 	= $this->input->post('jenkel');
		$tempat		= $this->input->post('tempat');
		$ttl 		= $this->input->post('ttl');
		$alamat 	= $this->input->post('alamat');
		$no_hp 		= $this->input->post('no_hp'); 
		$kelas 		= $this->input->post('kelas');
		$ta 		= $this->input->post('ta'); 

		$where = array('id_siswa' => $id); 

		$data = array(
			'no_induk_siswa' 	=> $no_induk,
			'nisn_siswa' 		=> $nisn,
			'nama_siswa' 		=> $nama,
			'jenkel_siswa' 		=> $jenkel,
			'tempat_siswa' 		=> $tempat,
			'tgl_lahir_siswa' 	=> $ttl,
			'alamat_siswa' 		=> $alamat,
			'no_hp_siswa' 		=> $no_hp, 
			'kode_kelas' 		=> $kelas,
			'tahun_ajaran'		=> $ta
		);

		if ($this->form_validation->run() == FALSE)
		{  
			redirect('a/s/update-data?p='.$id.'&notif=2');
		}
		else
		{ 
			$this->DBase->update_data('tb_siswa', $where, $data);
			redirect('a/s/data?notif=2');
		}  
	}  

	public function deleteDataSiswa()
	{ 
		$id_siswa = $this->input->get('p');
		$where = array('id_siswa' => $id_siswa); 

		$this->DBase->delete_data('tb_siswa', $where);
		redirect('a/s/data?notif=3');
	} 

	public function viewReportHarianSiswa()
	{	
		$kelas = $this->input->get('kode_kelas');
		$ta = $this->input->get('ta');
		$tanggal = $this->input->get('tanggal');  

		$where = array(
			'tb_siswa.kode_kelas' => $kelas,	
			'tb_kelas.kode_kelas' => $kelas,
			'tahun_ajaran' => $ta,
			'tanggal' => $tanggal 
		);

		
		$data['data_ta'] = $this->DBase->get_data('tb_ta', 'tahun_ajaran', 'DESC');
		$data['data_kelas'] = $this->DBase->get_data('tb_kelas', 'kode_kelas', 'DESC'); 
		$data['DATA'] = $this->DBase->get_report_harian_siswa($where);   
		$data['val_ta'] = $ta;
		$data['val_kelas'] = $kelas;
		$data['val_tanggal'] = $tanggal;
   
		$this->template->admin('admin/siswa/vreport_harian_siswa', $data); 
	} 

	public function viewReportHarianSiswaPrint()
	{	
		$kelas = $this->input->get('kelas');
		$ta = $this->input->get('ta');
		$tanggal = $this->input->get('tanggal'); 
 
		$where = array(
			'tb_siswa.kode_kelas' => $kelas,	
			'tb_kelas.kode_kelas' => $kelas,
			'tahun_ajaran' => $ta,
			'tanggal' => $tanggal 
		);
  
		$data['data_ta'] = $this->DBase->get_data('tb_ta', 'tahun_ajaran', 'DESC');
		$data['data_kelas'] = $this->DBase->get_data('tb_kelas', 'kode_kelas', 'DESC'); 
		$data['DATA'] = $this->DBase->get_report_harian_siswa($where);    
		
		$data['val_ta'] = "";
		$data['val_kelas'] = "";
		$data['val_tanggal'] = "";

		if($data['DATA']->num_rows() == 0){ 
			$this->template->admin('admin/siswa/vreport_harian_siswa', $data); 
		}else{
			$this->load->view('admin/siswa/vreport_harian_siswa_print', $data); 
		}
	} 
 
	public function viewReportBulananSiswa()
	{
		$ta = $this->input->get('ta'); 
		$kelas = $this->input->get('kelas');
		$month_year = $this->input->get('bulan');

		$data['data_absensi'] = array();
		$data['data_ta'] = $this->DBase->get_data('tb_ta', 'tahun_ajaran', 'DESC');
		$data['data_kelas'] = $this->DBase->get_data('tb_kelas', 'kode_kelas', 'DESC'); 
		$data['val_ta'] = $ta;
		$data['val_kelas'] = $kelas;
		$data['val_bulan'] = $month_year; 
		$data['val_month'] = date('m', strtotime('01-'.$month_year));
		$data['val_years'] = date('Y', strtotime('01-'.$month_year));

		if ($month_year) { 
			$month = date('m', strtotime('01-'.$month_year));
			$year = date('Y', strtotime('01-'.$month_year));
			$data['count_days'] = date('t', strtotime('01-'.$month_year));

			$employees = $this->DBase->get_siswa([
				'tb_ta.tahun_ajaran' => $ta,
				'tb_kelas.kode_kelas' => $kelas
				]);

			foreach ($employees->result() as $employee) {
				$absen = array();
				$absensi_employees = $this->DBase->get_where_data('tb_absensi_siswa', [
					'id_siswa' => $employee->id_siswa,
					'status' => 'H',
					'MONTH(tanggal)' => $data['val_month'],
					'YEAR(tanggal)' => $data['val_years']
				]);
				
				for ($i=1; $i <= $data['count_days']; $i++) { 
					$absen[$i] = ['absen' => false];
					foreach ($absensi_employees->result() as $absensi) {
						if ((date('d', strtotime($absensi->tanggal)) == $i) && $absensi->status == 'H') {
							$absen[$i] = ['absen' => true];
						}
					}
				}

				$data['data_absensi'][] = [
					'no_induk' => $employee->no_induk_siswa,
					'name' => $employee->nama_siswa,
					'kelas' => $employee->kode_kelas,
					'ta' => $employee->tahun_ajaran, 
					'absensi' => $absen
				];
			}
		} 
		
		$this->template->admin('admin/siswa/vreport_bulanan_siswa', $data);
	} 

	public function viewReportBulananSiswaPrint()
	{
		$ta = $this->input->get('ta'); 
		$kelas = $this->input->get('kelas');
		$month_year = $this->input->get('bulan');

		$data['data_absensi'] = array();
		$data['data_ta'] = $this->DBase->get_data('tb_ta', 'tahun_ajaran', 'DESC');
		$data['data_kelas'] = $this->DBase->get_data('tb_kelas', 'kode_kelas', 'DESC'); 
		$data['val_ta'] = $ta;
		$data['val_kelas'] = $kelas;
		$data['val_bulan'] = $month_year; 
		$data['val_month'] = date('m', strtotime('01-'.$month_year));
		$data['val_years'] = date('Y', strtotime('01-'.$month_year));

		if ($month_year) {  
			$data['count_days'] = date('t', strtotime('01-'.$month_year));

			$employees = $this->DBase->get_siswa([
				'tb_ta.tahun_ajaran' => $ta,
				'tb_kelas.kode_kelas' => $kelas
				]);

			foreach ($employees->result() as $employee) {
				$absen = array();
				$absensi_employees = $this->DBase->get_where_data('tb_absensi_siswa', [
					'id_siswa' => $employee->id_siswa,
					'status' => 'H',
					'MONTH(tanggal)' => $data['val_month'],
					'YEAR(tanggal)' => $data['val_years']
				]);
				
				for ($i=1; $i <= $data['count_days']; $i++) { 
					$absen[$i] = ['absen' => false];
					foreach ($absensi_employees->result() as $absensi) {
						if ((date('d', strtotime($absensi->tanggal)) == $i) && $absensi->status == 'H') {
							$absen[$i] = ['absen' => true];
						}
					}
				}

				$data['data_absensi'][] = [
					'no_induk' => $employee->no_induk_siswa,
					'name' => $employee->nama_siswa,
					'kelas' => $employee->kode_kelas,
					'ta' => $employee->tahun_ajaran,
					'absensi' => $absen
				];
			}
		}  

		if(count($data['data_absensi']) == 0){ 
			$data['val_ta'] = "";
			$data['val_kelas'] = "";
			$data['val_bulan'] = "";
			$this->template->admin('admin/siswa/vreport_bulanan_siswa', $data); 
		}else{
			$this->load->view('admin/siswa/vreport_bulanan_siswa_print', $data); 
		}  
	} 

	// Kelas Siswa
	public function viewAddKelas()
	{ 
        if ($this->input->get('notif') == 'a1') {
            $data['notif_gagal'] = "* Please fill in the form";
        }else{
            $data['notif_gagal'] = "";
        }

		$this->template->admin('admin/settings/siswa/vadd_kelas', $data);
	} 

	public function addkelas()
	{
		$this->form_validation->set_rules('kode_kelas', 'Kode Kelas', 'required');
		$this->form_validation->set_rules('tingkat', 'Tingkat', 'required');
		$this->form_validation->set_rules('nama_jurusan', 'Nama Jurusan', 'required'); 
		$this->form_validation->set_rules('kode_jadwal', 'Kode Jadwal', 'required'); 

		$kode_kelas 	= $this->input->post('kode_kelas');
		$tingkat 	= $this->input->post('tingkat');
		$nama_jurusan 	= $this->input->post('nama_jurusan');  
		$kode_jadwal 	= $this->input->post('kode_jadwal'); 

		$data = array(
			'kode_kelas' => $tingkat.'-'.$kode_kelas, 
			'nama_jurusan' => $nama_jurusan,
			'kode_jadwal' => $kode_jadwal, 
		); 

		if ($this->form_validation->run() == FALSE)
		{  
			redirect('s/k/insert?notif=a1');
		}
		else
		{ 
			$this->DBase->insert_data('tb_kelas', $data);
			
			redirect('s/siswa?notif=a1');
		}
	}  

	public function viewUpdateKelas()
	{ 
		$kode_kelas = $this->input->get('q');
		$where = array('kode_kelas' => $kode_kelas);
		$data['DATA'] = $this->DBase->get_where_data('tb_kelas', $where)->row(); 

        if ($this->input->get('notif') == 'a2') {
            $data['notif_gagal'] = "* Please fill in the form";
        }else{
            $data['notif_gagal'] = "";
        }

		$this->template->admin('admin/settings/siswa/vupdate_kelas', $data);
	} 

	public function updateKelas()
	{
		$this->form_validation->set_rules('kode_kelas', 'Kode kelas', 'required'); 
		$this->form_validation->set_rules('nama_jurusan', 'Nama kelas', 'required'); 
		$this->form_validation->set_rules('kode_jadwal', 'Kode Jadwal', 'required'); 

		$kode_kelas 	= $this->input->post('kode_kelas'); 
		$nama_jurusan 	= $this->input->post('nama_jurusan');  
		$kode_jadwal 	= $this->input->post('kode_jadwal'); 

		$where = array('kode_kelas' => $kode_kelas);

		$data = array(  
			'nama_jurusan' => $nama_jurusan, 
			'kode_jadwal' => $kode_jadwal, 
		); 

		if ($this->form_validation->run() == FALSE)
		{  
			redirect('s/k/update?notif=a2&q='.$kode_kelas);
		}
		else
		{ 
			$this->DBase->update_data('tb_kelas', $where, $data);
			redirect('s/siswa?notif=a2');
		}
	}   

	public function deleteKelas()
	{ 
		$kode_kelas = $this->input->get('q');
		$where = array('kode_kelas' => $kode_kelas); 

		$this->DBase->delete_data('tb_kelas', $where);
		redirect('s/siswa?notif=a3');
	} 
	
	// Jadwal Siswa
	public function viewUpdateJadwal()
	{ 
		$kode_jadwal = $this->input->get('q');
		$where = array('kode_jadwal' => $kode_jadwal);
		$data['DATA'] = $this->DBase->get_where_data('tb_jadwal_siswa', $where)->row(); 

        if ($this->input->get('notif') == 'a2') {
            $data['notif_gagal'] = "* Please fill in the form";
        }else{
            $data['notif_gagal'] = "";
        }

		$this->template->admin('admin/settings/siswa/vupdate_jadwal', $data);
	} 

	public function updateJadwal()
	{
		$this->form_validation->set_rules('kode_jadwal', 'Kode Jadwal', 'required');
		$this->form_validation->set_rules('time_in', 'Time In', 'required'); 
		$this->form_validation->set_rules('time_in_', 'Time In_', 'required');  

		$kode_jadwal 	= $this->input->post('kode_jadwal');
		$time_in 		= $this->input->post('time_in'); 
		$time_in_ 		= $this->input->post('time_in_');   

		$where = array('kode_jadwal' => $kode_jadwal);

		$data = array( 
			'time_in' 	=> $time_in, 
			'time_in_' 	=> $time_in_ 
		); 

		if ($this->form_validation->run() == FALSE)
		{  
			redirect('s/j/update?notif=a2');
		}
		else
		{ 
			$this->DBase->update_data('tb_jadwal_siswa', $where, $data);
			redirect('s/siswa?notif=a2');
		}
	}   

	public function deleteJadwal()
	{ 
		$kode_jadwal = $this->input->get('q');
		$where = array('kode_jadwal' => $kode_jadwal); 

		$this->DBase->delete_data('tb_jadwal_siswa', $where);
		redirect('s/siswa?notif=a3');
	} 

	// Kelas TA
	public function viewAddTA()
	{ 
        if ($this->input->get('notif') == 'a1') {
            $data['notif_gagal'] = "* Please fill in the form";
        }else{
            $data['notif_gagal'] = "";
        }

		$this->template->admin('admin/settings/siswa/vadd_ta', $data);
	} 

	public function addTA()
	{
		$this->form_validation->set_rules('ta', 'Tahun Ajaran', 'required'); 

		$ta 	= $this->input->post('ta'); 

		$data = array(
			'tahun_ajaran' => $ta  
		); 

		if ($this->form_validation->run() == FALSE)
		{  
			redirect('s/t/insert?notif=a1');
		}
		else
		{ 
			$this->DBase->insert_data('tb_ta', $data);
			
			redirect('s/siswa?notif=a1');
		}
	}  

	public function viewUpdateTA()
	{ 
		$ta = $this->input->get('q');
		$where = array('tahun_ajaran' => $ta);
		$data['DATA'] = $this->DBase->get_where_data('tb_ta', $where)->row(); 

        if ($this->input->get('notif') == 'a2') {
            $data['notif_gagal'] = "* Please fill in the form";
        }else{
            $data['notif_gagal'] = "";
        }

		$this->template->admin('admin/settings/siswa/vupdate_ta', $data);
	} 
 
	public function deleteTA()
	{ 
		$ta = $this->input->get('q');
		$where = array('tahun_ajaran' => $ta); 

		$this->DBase->delete_data('tb_ta', $where);
		redirect('s/siswa?notif=a3');
	} 
}
