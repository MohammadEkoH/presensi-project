<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Absen extends CI_Controller { 
  
    public function showAlat()
    {
        $query = $this->DBase->get_data('tb_alat', 'nama_alat', 'ASC')->result(); 
        $data = array();
        foreach ($query as $row) {
            $data[] = array(
                'nama_alat' => $row->nama_alat,
                'kode_token'  => $row->kode_token,
                'time_old'  => $row->time_old,
                'status_alat' => $row->status_alat,
            );
        }

        echo json_encode($data); 
    }

    public function storeTimeNow()
    {
        $token = $this->input->get('token');
        $where = array('kode_token' => $token);
        $DataTime = array( 
            'tgl_now' => date('Y-m-d'),
            'time_now' => date('H:i:s')
        );
        $this->DBase->update_data('tb_alat', $where, $DataTime);    
        sleep(1);
        redirect('dashboard'); 
    }
    
    public function statusAlat()
    {
        $token = $this->input->get('token');
        $where = array('kode_token' => $token);
        $data_time_old = array( 
            'tgl_old' => date('Y-m-d'),
            'time_old' => date('H:i:s')
        );
        
        $this->DBase->update_data('tb_alat', $where, $data_time_old);

        $query = $this->DBase->get_where_data('tb_alat', $where)->row(); 
        if ($query->tgl_now == $query->tgl_old && $query->time_now < $query->time_old) { 
            $whereStatus = array( 
                'status_alat' => 1
            ); 
        }else{
            $whereStatus = array( 
                'status_alat' => 0
            ); 
        }
        $this->DBase->update_data('tb_alat', $where, $whereStatus); 
    } 

	public function storeSiswa()
    { 
        $employees = $this->DBase->get_data('tb_siswa')->result();
        $data = array();
        foreach ($employees as $employee) {
            $data = array(
				'id_absensi' => random_string('alnum', 10),
                'id_siswa' => $employee->id_siswa, 
				'keterlambatan' => 0,
				'tanggal' => date('Y-m-d'),
				'ta'	=> $employee->tahun_ajaran,
                'status' => 'A'
            );

			$absensi = $this->DBase->get_where_data('tb_absensi_siswa', 
													['tanggal' => date('Y-m-d'),  					 
													'id_siswa' => $employee->id_siswa])->num_rows(); 
            if ($absensi < 1) {
				$this->DBase->insert_data('tb_absensi_siswa', $data); 
				echo "berhasil";
            }else {
                continue;
            }
        }
    } 

    public function getDataUserSiswa()
    {    
        $uri = $this->uri->segment(2);
        switch ($uri) {
            case 'cek': 
                $id = $this->input->get('ID'); 

                if (isset($id)) {
                    $where_id = array('id_siswa' => $id); 
                    $cek_id = $this->DBase->get_where_data('tb_siswa', $where_id)->row();
                    // echo $cek_id->id_siswa;die;
                    if ($cek_id->id_siswa) {
                        $data_status_smart_card = array('status_smart_card' => 1);
                        $this->DBase->update_data('tb_siswa', $where_id, $data_status_smart_card);

                        $cek = $this->DBase->get_where_data('tb_siswa', $where_id)->row();
                        // echo $cek->status_smart_card;die;
                        if($cek->status_smart_card > 0){ 
                            echo "done";
                        }else{
                            echo "fail";
                        }    
                    }else{
                        echo "fail";
                    }
 
                }else{
                    echo "fail";
                }
                break;
            
            default :
                $token = $this->input->get('TOKEN'); 
                $where_token = array('kode_token' => $token);
                $data_token = $this->DBase->get_where_data('tb_alat', $where_token)->num_rows(); 
                if ($data_token > 0) {
                    // ERROR JIKA SUDAH TIDAK ADA DATA DENGAN STATUS_SMART_CARD YANG 0 m
                    $where_status = array('status_smart_card' => 0); 
                    $getWhereData = $this->DBase->get_where_data_siswa($where_status);  
                    if ($getWhereData->num_rows() > 0) {
                        if ($getWhereData->row()->status_smart_card == 0) { 
                            $data = array(
                                'ID'            => $getWhereData->row()->id_siswa, 
                                'NAMA'          => $getWhereData->row()->nama_siswa,
                                'STATUSCard'    => $getWhereData->row()->status_smart_card,
                                'JADWAL'        => $getWhereData->row()->kode_jadwal,
                                'OK'            => "OK"
                            );  
        
                            echo json_encode($data); 
                        } else { 
                            echo "fail";  
                        }  
                    }else {
                        echo "gagal";
                    } 
                } 
                break;
        }
         
    }

    public function storeAbsensiSiswa()
	{ 
		$status = $this->input->get('status');
		$token = $this->input->get('token');
		$id_siswa = $this->input->get('uid');
		$kode_jadwal = $this->input->get('kode_jadwal');

		$where_token = array('kode_token' => $token);
		$cek_alat = $this->DBase->get_where_data('tb_alat', $where_token)->num_rows();

		$where_id_siswa = array('id_siswa' => $id_siswa);
        $cek_id_siswa = $this->DBase->get_where_data('tb_siswa',$where_id_siswa); 
         
        $where_tgl = array('id_siswa' => $id_siswa, 'tanggal' => date('Y-m-d'));
		$cek_tgl = $this->DBase->get_where_data('tb_absensi_siswa', $where_tgl)->row();
 

        $time_now = date('H:i:s'); 
        // $time_now = '15:00:00';  
		$time_where = array('kode_jadwal' => $kode_jadwal);
        $time_data = $this->DBase->get_where_data('tb_jadwal_siswa', $time_where)->row(); 
		
		$where_id = array(
			'id_siswa' => $id_siswa,
			'tanggal' => date('Y:m:d'), 
        );  
        
        // echo $time_data->kode_jadwal; die;

		if ($cek_alat > 0) {
			if ($cek_id_siswa->num_rows() > 0) {
                if ($cek_tgl->jam_masuk == '00:00:00') { 
                    if ($time_now >= $time_data->time_in  &&  $time_now <= $time_data->time_in_) {
                        $data = array( 
                            'jam_masuk' => date('H:i:s'), 
                            'keterlambatan' => 0,
                            'status' => 'H'
                        ); 
        
                        $this->DBase->update_data('tb_absensi_siswa', $where_id, $data);
        
                        echo "oke";
                    }else{
                        echo "time_fail";
                    }
                }else{
                    echo  "done";
                } 
			}else{
				echo  "id_fail";
			}
			 
		}else{
			echo "token_fail";
		} 
    }

    public function storePegawai()
    { 
        $employees = $this->DBase->get_data('tb_pegawai')->result();
        $data = array();
        foreach ($employees as $employee) {
            $data = array(
				'id_absensi' => random_string('alnum', 10),
                'id_pegawai' => $employee->id_pegawai, 
				'keterlambatan' => 0,
				'tanggal' => date('Y-m-d'), 
                'status_masuk' => 'A',
                'status_keluar' => 'A'
            );

			$absensi = $this->DBase->get_where_data('tb_absensi_pegawai', 
													['tanggal' => date('Y-m-d'),  					 
													'id_pegawai' => $employee->id_pegawai])->num_rows(); 
            if ($absensi < 1) {
				$this->DBase->insert_data('tb_absensi_pegawai', $data); 
				echo "berhasil";
            }else {
                continue;
            }
        }
    } 

    public function getDataUserPegawai()
    {    
        $uri = $this->uri->segment(2);
        switch ($uri) {
            case 'cek': 
                $id = $this->input->get('ID'); 

                if (isset($id)) {
                    $where_id = array('id_pegawai' => $id); 
                    $cek_id = $this->DBase->get_where_data('tb_pegawai', $where_id)->row();
                    // echo $cek_id->id_pegawai;die;
                    if ($cek_id->id_pegawai) {
                        $data_status_smart_card = array('status_smart_card' => 1);
                        $this->DBase->update_data('tb_pegawai', $where_id, $data_status_smart_card);

                        $cek = $this->DBase->get_where_data('tb_pegawai', $where_id)->row();
                        // echo $cek->status_smart_card;die;
                        if($cek->status_smart_card > 0){ 
                            echo "done";
                        }else{
                            echo "fail";
                        }    
                    }else{
                        echo "fail";
                    }
 
                }else{
                    echo "fail";
                }
                break;
            
            default :
                $token = $this->input->get('TOKEN'); 
                $where_token = array('kode_token' => $token); 
                $data_token = $this->DBase->get_where_data('tb_alat', $where_token)->num_rows(); 
                if ($data_token > 0) {
                    // ERROR JIKA SUDAH TIDAK ADA DATA DENGAN STATUS_SMART_CARD YANG 0 m
                    $where_status = array('status_smart_card' => 0); 
                    $getWhereData = $this->DBase->get_where_data('tb_pegawai', $where_status);
                    if ($getWhereData->num_rows() > 0) {
                        if ($getWhereData->row()->status_smart_card == 0) { 
                            $data = array(
                                'ID'            => $getWhereData->row()->id_pegawai, 
                                'NAMA'          => $getWhereData->row()->nama_pegawai,
                                'STATUSCard'    => $getWhereData->row()->status_smart_card,
                                'JADWAL'        => "-",
                                'OK'            => "OK"
                            );  
        
                            echo json_encode($data); 
                        } else { 
                            echo "fail";  
                        }  
                    }else {
                        echo "gagal";
                    } 
                } 
                break;
        }
         
    } 
    
    public function storeAbsensiPegawai()
	{ 
		$btn = $this->input->get('btn');
		$status = $this->input->get('status');
		$token = $this->input->get('token');
		$id_pegawai = $this->input->get('uid');
		$kode_jadwal = $this->input->get('kode_jadwal');

		$where_token = array('kode_token' => $token);
		$cek_alat = $this->DBase->get_where_data('tb_alat', $where_token)->num_rows();

		$where_id_pegawai = array('id_pegawai' => $id_pegawai);
        $cek_id_pegawai = $this->DBase->get_where_data('tb_pegawai', $where_id_pegawai)->num_rows();
         
        $where_tgl = array('id_pegawai' => $id_pegawai, 'tanggal' => date('Y-m-d'));
        $cek_tgl = $this->DBase->get_where_data('tb_absensi_pegawai', $where_tgl)->row(); 

        $time_now = date('H:i:s'); 
        // $time_now = '15:00:00';  
		$time_data = $this->DBase->get_data('tb_jadwal_pegawai')->row();
		
		$where_id = array(
			'id_pegawai' => $id_pegawai,
			'tanggal' => date('Y:m:d'), 
        );  
        
		if ($cek_alat > 0) {
			if ($cek_id_pegawai > 0) {
                if ($cek_tgl->jam_masuk == '00:00:00' || $cek_tgl->jam_keluar == '00:00:00') {
                    if ($cek_tgl->jam_masuk == '00:00:00' && $time_now >= $time_data->time_in  &&  $time_now <= $time_data->time_in_) {
                        $data = array( 
                            'jam_masuk' => date('H:i:s'), 
                            'keterlambatan' => 0,
                            'status_masuk' => 'H'
                        ); 
        
                        $this->DBase->update_data('tb_absensi_pegawai', $where_id, $data);
        
                        echo "oke";
                    }else if ($cek_tgl->jam_masuk != '00:00:00' && $cek_tgl->jam_keluar == '00:00:00' && $time_now >= $time_data->time_out  &&  $time_now <= $time_data->time_out_) {
                        $data = array( 
                            'jam_keluar' => date('H:i:s'), 
                            'keterlambatan' => 0,
                            'status_keluar' => 'H'
                        ); 
        
                        $this->DBase->update_data('tb_absensi_pegawai', $where_id, $data);
        
                        echo "oke";
                    }else{
                        echo "time_fail";
                    }  
                }else{
                    echo  "done";
                } 
			}else{
				echo  "id_fail";
			}
			 
		}else{
			echo "token_fail";
		} 
	}
}
