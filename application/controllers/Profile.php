<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {
 
	public function index()
	{   
		$data['DATA'] = $this->DBase->get_data('tb_alat', 'nama_alat', 'ASC')->result();
 
		$this->template->admin('admin/vprofile', $data);
	} 
}
