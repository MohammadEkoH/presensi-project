<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai extends CI_Controller { 

	public function __construct()
    {
        parent::__construct();  
        if (!$this->ion_auth->logged_in()) {//cek login ga?
    		redirect('auth','refresh');
    	}else{
            if (!$this->ion_auth->in_group('admin')) {//cek admin ga?
                redirect('auth','refresh');
            }
        }
	}

	public function editStatusCardPegawai()
    {
        $id_pegawai = $this->input->get('id_pegawai');
        $where_id = array('id_pegawai' => $id_pegawai);
        $status = $this->input->get('status');
        
        if ($status == 1) {
            $data = array('status_smart_card' => 0);
        }else{
            $data = array('status_smart_card' => 1);
        }

        $this->DBase->update_data('tb_pegawai', $where_id, $data);
		
		redirect('a/p/data');
    }
	
	public function viewDataPegawai()
	{  
		$data['data_jabatan'] = $this->DBase->get_data('tb_jabatan')->result();
		$data['count'] = $this->DBase->get_data('tb_pegawai')->num_rows();
 
		//pagination settings
		$config['base_url'] = site_url('a/p/data');
		$config['total_rows'] = $data['count'];
		$config['per_page'] = "10";
		$config["uri_segment"] = 4;
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = floor($choice);
  
		//config for bootstrap pagination class integration
		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination pagination-sm no-margin pull-right">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';
  
		$this->pagination->initialize($config);
  
		$data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
  
		//call the model function to get the department data
		$data['data_pegawai'] = $this->DBase->get_data_pegawai($config['per_page'], $data['page']);
  
		$data['pagination'] = $this->pagination->create_links();

		if ($this->input->get('notif') == 1) { 
			$data['notif_berhasil'] = "	<div class='alert alert-success alert-dismissible'>
											<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
											<h4><i class='icon fa fa-check'></i> Successfully Adding New Data!</h4> 
										</div>";
		}else if ($this->input->get('notif') == 2) { 
			$data['notif_berhasil'] = "	<div class='alert alert-warning alert-dismissible'>
											<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
											<h4><i class='icon fa fa-check'></i> Successfully Update Data!</h4> 
										</div>";
		}else if ($this->input->get('notif') == 3) { 
			$data['notif_berhasil'] = "	<div class='alert alert-danger alert-dismissible'>
											<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
											<h4><i class='icon fa fa-check'></i> Successfully Delete Data!</h4> 
										</div>";
		}else{
			$data['notif_berhasil'] = "";
		} 

		$data['notif_limit'] = "";

		$this->template->admin('admin/pegawai/vdataPegawai', $data);
	} 

	public function searchDataPegawai()
	{ 
		$nama = $this->input->post('nama');
		$jabatan = $this->input->post('jabatan');  
		 
		$data['data_jabatan'] = $this->DBase->get_data('tb_jabatan')->result(); 
		$data['count'] = $this->DBase->count_data_pegawai($jabatan)->num_rows();
  
		$limit = 10; 
		
		$start = 0;
  
		//call the model function to get the department data
		$data['data_pegawai'] = $this->DBase->search_data_pegawai($nama, $jabatan, $limit, $start);
	 
		$data['pagination'] = ""; 
		$data['notif_berhasil'] = ""; 

		if ($data['count'] == 0) {
			$data['notif_limit'] = "* Data Tidak ditemukan ";
		}else{
			$data['notif_limit'] = "* Pencarian dibatasi ". $limit;
		}
 
		$this->template->admin('admin/pegawai/vdataPegawai', $data);
	} 

	public function viewAddDataPegawai()
	{ 
		$data['data_jabatan'] = $this->DBase->get_data('tb_jabatan')->result();

		if ($this->input->get('notif') == 1) { 
			$data['notif_gagal'] = "Please fill in the form";
		}else{
			$data['notif_gagal'] = "";
		}

		$this->template->admin('admin/pegawai/vadd_data_pegawai', $data);
	}  
	
	public function addDataPegawai()
	{ 
		$query = $this->DBase->get_id('tb_pegawai', 'id_pegawai');
		$id = $query->row();
		$id = $id->id_pegawai;

		$id_pegawai = substr($id, 7, 8);
		if ($id_pegawai == 0) {
			$id_pegawai = 1;
		}else {
			$id_pegawai++;
		}

		$this->form_validation->set_rules('no_induk', 'No. Induk', 'required');
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required'); 
		$this->form_validation->set_rules('jenkel', 'Jenis Kelamin', 'required'); 
		$this->form_validation->set_rules('jabatan', 'Jabatan', 'required');

		$id			= $this->kode->kode_ID(random_string('alnum', 7), $id_pegawai, $query); 
		$no_induk 	= $this->input->post('no_induk');
		$nama 		= $this->input->post('nama');
		$email 		= $this->input->post('email');
		$jenkel 	= $this->input->post('jenkel'); 
		$jabatan 	= $this->input->post('jabatan');
 
		$data = array(
			'id_pegawai'		=> $id,
			'no_induk_pegawai' 	=> $no_induk,
			'nama_pegawai' 		=> $nama, 
			'email_pegawai' 	=> $email, 
			'jenkel_pegawai' 	=> $jenkel, 
			'kode_pegawai' 		=> $jabatan
		);

		if ($this->form_validation->run() == FALSE)
		{ 
			redirect('a/p/insert-data?notif=1', $data);
		}
		else
		{ 
			$this->DBase->insert_data('tb_pegawai', $data);
			
			redirect('a/p/data?notif=1', $data);
		}  
    }  
    
    public function viewUpdateDataPegawai()
	{
        $id_pegawai = $this->input->get('p');
        $where = array('id_pegawai' => $id_pegawai);
 
		$data['data_jabatan'] = $this->DBase->get_data('tb_jabatan')->result();
        $data['DATA'] = $this->DBase->get_where_data('tb_pegawai', $where)->row();  

		if ($this->input->get('notif') == 2) { 
			$data['notif_gagal'] = "Please fill in the form";
		}else{
			$data['notif_gagal'] = "";
		}

		$this->template->admin('admin/pegawai/vupdate_data_pegawai', $data);
	}    

	public function updateDataPegawai()
	{
		$this->form_validation->set_rules('id', 'ID', 'required');
		$this->form_validation->set_rules('no_induk', 'No. Induk', 'required');
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required'); 
		$this->form_validation->set_rules('jenkel', 'Jenis Kelamin', 'required'); 
		$this->form_validation->set_rules('jabatan', 'Jabatan', 'required');

		$id 		= $this->input->post('id');
		$no_induk 	= $this->input->post('no_induk');
		$nama 		= $this->input->post('nama');
		$email 		= $this->input->post('email');
		$jenkel 	= $this->input->post('jenkel'); 
		$jabatan 	= $this->input->post('jabatan');

		$where = array('id_pegawai' => $id);

		$data = array(
			'no_induk_pegawai' 	=> $no_induk,
			'nama_pegawai' 		=> $nama, 
			'email_pegawai' 	=> $email, 
			'jenkel_pegawai' 	=> $jenkel, 
			'kode_pegawai'	 	=> $jabatan
		);

		if ($this->form_validation->run() == FALSE)
		{ 
			redirect('a/p/update-data?p='.$id.'&notif=2');
		}
		else
		{ 
			$this->DBase->update_data('tb_pegawai', $where, $data);
			
			redirect('a/p/data?notif=2');
		}  
	}  

	public function deleteDataPegawai()
	{ 
		$id = $this->input->get('p');
		$where = array('id_pegawai' => $id); 

		$this->DBase->delete_data('tb_pegawai', $where);
		redirect('a/p/data?notif=3');
	} 

	public function viewReportHarianPegawai()
	{	
		$kode_pegawai = $this->input->get('kode_pegawai'); 
		$tanggal = $this->input->get('tanggal'); 
 
		$whereTgl = array('tanggal' => $tanggal);

		// var_dump($whereTgl);

		$data['data_jabatan'] = $this->DBase->get_data('tb_jabatan', 'kode_pegawai', 'ASC')->result();
		$data['DATA'] = $this->DBase->get_report_harian_pegawai($kode_pegawai, $whereTgl); 
		$data['val_tanggal_pegawai'] = $tanggal;
		
		$this->template->admin('admin/pegawai/vreport_harian_pegawai', $data);
	} 

	public function viewReportHarianPegawaiPrint()
	{	
		$kode_pegawai = $this->input->get('kode_pegawai'); 
		$tanggal = $this->input->get('tanggal'); 
 
		$whereTgl = array('tanggal' => $tanggal);

		$data['data_jabatan'] = $this->DBase->get_data('tb_jabatan', 'kode_pegawai', 'ASC')->result();
		$data['DATA'] = $this->DBase->get_report_harian_pegawai($kode_pegawai, $whereTgl);
		
		if($data['DATA']->num_rows() == 0){ 
			$data['val_tanggal_pegawai'] = "";
			$this->template->admin('admin/pegawai/vreport_harian_pegawai', $data); 
		}else{
			$this->load->view('admin/pegawai/vreport_harian_pegawai_print', $data); 
		}
	} 
 
	public function viewReportBulananPegawai()
	{
		$month_year = $this->input->get('bulan'); 
		
		$data['data_absensi'] = array();
		$data['val_tanggal_pegawai'] = $month_year;
		$data['val_month'] = date('m', strtotime('01-'.$month_year));
		$data['val_years'] = date('Y', strtotime('01-'.$month_year));

		if ($month_year) { 
			$data['count_days'] = date('t', strtotime('01-'.$month_year));

			$employees = $this->DBase->get_pegawai();

			foreach ($employees->result() as $employee) {
				$absen = array();
				$absensi_employees = $this->DBase->get_where_data('tb_absensi_pegawai', [
					'id_pegawai' => $employee->id_pegawai,
					'status_keluar' => 'H',
					'MONTH(tanggal)' => $data['val_month'],
					'YEAR(tanggal)' => $data['val_years']
				]);
				
				for ($i=1; $i <= $data['count_days']; $i++) { 
					$absen[$i] = ['absen' => false];
					foreach ($absensi_employees->result() as $absensi) {
						if ((date('d', strtotime($absensi->tanggal)) == $i) && $absensi->status_keluar == 'H') {
							$absen[$i] = ['absen' => true];
						}
					}
				}

				$data['data_absensi'][] = [
					'no_induk' => $employee->no_induk_pegawai,
					'name' => $employee->nama_pegawai,
					'jabatan' => $employee->jabatan_pegawai,
					'absensi' => $absen
				];
			}
		}
 
		$this->template->admin('admin/pegawai/vreport_bulanan_pegawai', $data);
	} 

	public function viewReportBulananPegawaiPrint()
	{ 
		$month_year = $this->input->get('tanggal'); 
		
		$data['data_absensi'] = array();
		$data['val_tanggal_pegawai'] = $month_year;
		$data['val_month'] = date('m', strtotime('01-'.$month_year));
		$data['val_years'] = date('Y', strtotime('01-'.$month_year));
 
		if ($month_year) { 
			$data['count_days'] = date('t', strtotime('01-'.$month_year));

			$employees = $this->DBase->get_pegawai();

			foreach ($employees->result() as $employee) {
				$absen = array();
				$absensi_employees = $this->DBase->get_where_data('tb_absensi_pegawai', [
					'id_pegawai' => $employee->id_pegawai,
					'status_keluar' => 'H',
					'MONTH(tanggal)' => $data['val_month'],
					'YEAR(tanggal)' => $data['val_years']
				]);
				
				for ($i=1; $i <= $data['count_days']; $i++) { 
					$absen[$i] = ['absen' => false];
					foreach ($absensi_employees->result() as $absensi) {
						if ((date('d', strtotime($absensi->tanggal)) == $i) && $absensi->status_keluar == 'H') {
							$absen[$i] = ['absen' => true];
						}
					}
				}

				$data['data_absensi'][] = [
					'no_induk' => $employee->no_induk_pegawai,
					'name' => $employee->nama_pegawai,
					'jabatan' => $employee->jabatan_pegawai,
					'absensi' => $absen
				];
			}
		}

		// var_dump(); die;

		if(count($data['data_absensi']) == 0){ 
			$data['val_tanggal_pegawai'] = "";
			$this->template->admin('admin/pegawai/vreport_bulanan_pegawai', $data); 
		}else{
			$this->load->view('admin/pegawai/vreport_bulanan_pegawai_print', $data); 
		} 
	} 

	// Jabatan Pegawai
	public function viewAddJabatan()
	{ 
        if ($this->input->get('notif') == 'a1') {
            $data['notif_gagal'] = "* Please fill in the form";
        }else{
            $data['notif_gagal'] = "";
        }

		$this->template->admin('admin/settings/pegawai/vadd_jabatan', $data);
	} 

	public function addJabatan()
	{
		$this->form_validation->set_rules('kode_pegawai', 'Kode Pegawai', 'required');
		$this->form_validation->set_rules('jabatan_pegawai', 'Jabatan Pegawai', 'required'); 

		$kode_pegawai 	= $this->input->post('kode_pegawai');
		$jabatan_pegawai = $this->input->post('jabatan_pegawai'); 

		$data = array(
			'kode_pegawai' => strtoupper($kode_pegawai), 
			'jabatan_pegawai' => strtoupper($jabatan_pegawai) 
		); 

		if ($this->form_validation->run() == FALSE)
		{  
			redirect('s/jb/insert?notif=a1');
		}
		else
		{ 
			$this->DBase->insert_data('tb_jabatan', $data);
			
			redirect('s/pegawai?notif=a1');
		}
	}  

	public function viewUpdateJabatan()
	{ 
		$kode_jabatan = $this->input->get('q');
		$where = array('kode_pegawai' => $kode_jabatan);
		$data['DATA'] = $this->DBase->get_where_data('tb_jabatan', $where)->row(); 

        if ($this->input->get('notif') == 'a2') {
            $data['notif_gagal'] = "* Please fill in the form";
        }else{
            $data['notif_gagal'] = "";
        }

		$this->template->admin('admin/settings/pegawai/vupdate_jabatan', $data);
	} 

	public function updateJabatan()
	{
		$this->form_validation->set_rules('kode_pegawai', 'Kode Pegawai', 'required');
		$this->form_validation->set_rules('jabatan_pegawai', 'Jabatan Pegawai', 'required'); 

		$kode_pegawai 	= $this->input->post('kode_pegawai');
		$jabatan_pegawai 	= $this->input->post('jabatan_pegawai'); 

		$where = array('kode_pegawai' => $kode_pegawai);

		$data = array( 
			'jabatan_pegawai' => $jabatan_pegawai, 
		); 

		if ($this->form_validation->run() == FALSE)
		{  
			redirect('s/jb/update?notif=a2');
		}
		else
		{ 
			$this->DBase->update_data('tb_jabatan', $where, $data);
			redirect('s/pegawai?notif=a2');
		}
	}   

	public function deleteJabatan()
	{ 
		$kode_pegawai = $this->input->get('q');
		$where = array('kode_pegawai' => $kode_pegawai); 

		$this->DBase->delete_data('tb_jabatan', $where);
		redirect('s/pegawai?notif=a3');
	} 

	// Jadwal Pegawai
	public function viewUpdateJadwal()
	{ 
		$kode_jadwal = $this->input->get('q');
		$where = array('kode_jadwal' => $kode_jadwal);
		$data['DATA'] = $this->DBase->get_where_data('tb_jadwal_pegawai', $where)->row(); 

        if ($this->input->get('notif') == 'a2') {
            $data['notif_gagal'] = "* Please fill in the form";
        }else{
            $data['notif_gagal'] = "";
        }

		$this->template->admin('admin/settings/pegawai/vupdate_jadwal', $data);
	} 

	public function updateJadwal()
	{
		$this->form_validation->set_rules('kode_jadwal', 'Kode Jadwal', 'required');
		$this->form_validation->set_rules('time_in', 'Time In', 'required'); 
		$this->form_validation->set_rules('time_in_', 'Time In_', 'required'); 
		$this->form_validation->set_rules('time_out', 'Time Out', 'required'); 
		$this->form_validation->set_rules('time_out_', 'Time Out_', 'required'); 

		$kode_jadwal 	= $this->input->post('kode_jadwal');
		$time_in 		= $this->input->post('time_in'); 
		$time_in_ 		= $this->input->post('time_in_'); 
		$time_out 		= $this->input->post('time_out'); 
		$time_out_ 		= $this->input->post('time_out_'); 

		$where = array('kode_jadwal' => $kode_jadwal);

		$data = array( 
			'time_in' 	=> $time_in, 
			'time_in_' 	=> $time_in_, 
			'time_out' 	=> $time_out, 
			'time_out_' => $time_out_
		); 

		if ($this->form_validation->run() == FALSE)
		{  
			redirect('s/jp/update?notif=a2');
		}
		else
		{ 
			$this->DBase->update_data('tb_jadwal_pegawai', $where, $data);
			redirect('s/pegawai?notif=a2');
		}
	}   

	public function deleteJadwal()
	{ 
		$kode_jadwal = $this->input->get('q');
		$where = array('kode_jadwal' => $kode_jadwal); 

		$this->DBase->delete_data('tb_jadwal_pegawai', $where);
		redirect('s/pegawai?notif=a3');
	} 
}
