<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Import extends CI_Controller { 

	public function __construct()
    {
        parent::__construct();  
        if (!$this->ion_auth->logged_in()) {//cek login ga?
    		redirect('auth','refresh');
    	}else{
            if (!$this->ion_auth->in_group('admin')) {//cek admin ga?
                redirect('auth','refresh');
            }
        }
    }

    function viewImportDataSiswa()
    {
		$data = "";
        $this->template->admin('admin/siswa/vsiswa_import', $data);
    } 
    
    function importSiswa()
    {
        if(isset($_FILES["file"]["name"]))
        {
            $path = $_FILES["file"]["tmp_name"];
            $object = PHPExcel_IOFactory::load($path);
            foreach($object->getWorksheetIterator() as $worksheet)
            {
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                for($row=2; $row<=$highestRow; $row++)
                {
                    $query = $this->DBase->get_id('tb_siswa', 'id_siswa');
                    $id = $query->row();
                    $id = $id->id_siswa;

                    $id_siswa = substr($id, 7, 8);
                    if ($id_siswa == 0) {
                        $id_siswa = 1;
                    }else {
                        $id_siswa++;
                    }
                    $id = $this->kode->kode_ID(random_string('alnum', 7), $id_siswa, $query); 
                    $no_induk = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                    $nisn = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                    $nama = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                    $jenkel = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                    $tempat = $worksheet->getCellByColumnAndRow(4, $row)->getValue(); 
                    $ttl = $worksheet->getCellByColumnAndRow(5, $row)->getValue();  
                    $convertTtl = date('Y-m-d', ($ttl - 25569) * 86400); 
                    $alamat = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
                    $no_hp = $worksheet->getCellByColumnAndRow(7, $row)->getValue(); 
                    $ta = $worksheet->getCellByColumnAndRow(8, $row)->getValue(); 
                    $kode_kelas = $worksheet->getCellByColumnAndRow(9, $row)->getValue(); 

                    $data[] = array(
                        'id_siswa' 	    => $id, 
                        'no_induk_siswa'=> $no_induk, 
                        'nisn_siswa'    => $nisn, 
                        'nama_siswa'   	=> ucwords($nama), 
                        'jenkel_siswa'  => strtoupper($jenkel),
                        'tempat_siswa'  => $tempat,
                        'tgl_lahir_siswa' => $convertTtl,
                        'alamat_siswa'  => $alamat,
                        'no_hp_siswa'  	=> $no_hp, 
                        'tahun_ajaran'  => $ta,
                        'kode_kelas'  	=> strtoupper($kode_kelas), 
                        'status_smart_card'	=> 0
                    );
                }
            }
            // print_r($data);
            $this->DBase->insert_data_batch('tb_siswa', $data);
            redirect('a/s/data');
        }
    }

    function viewImportDataPegawai()
    {
		$data = "";
        $this->template->admin('admin/pegawai/vpegawai_import', $data);
    } 
    
    function importPegawai()
    {
        if(isset($_FILES["file"]["name"])){
            
            $path = $_FILES["file"]["tmp_name"];
            $object = PHPExcel_IOFactory::load($path);
            foreach($object->getWorksheetIterator() as $worksheet)
            {
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                for($row=2; $row<=$highestRow; $row++)
                {
                    $query = $this->DBase->get_id('tb_pegawai', 'id_pegawai');
                    $id = $query->row();
                    $id = $id->id_pegawai;

                    $id_pegawai = substr($id, 7, 8);
                    if ($id_pegawai == 0) {
                        $id_pegawai = 1;
                    }else {
                        $id_pegawai++;
                    }
                    $id	= $this->kode->kode_ID(random_string('alnum', 7), $id_pegawai, $query); 
                    $no_induk = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                    $nama = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                    $email = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                    $jenkel = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                    $kode_pegawai = $worksheet->getCellByColumnAndRow(4, $row)->getValue(); 

                    $data[] = array(
                        'id_pegawai'        => $id,
                        'no_induk_pegawai' 	=> $no_induk, 
                        'nama_pegawai'   	=> ucwords($nama), 
                        'email_pegawai'  	=> $email,
                        'jenkel_pegawai'  	=> strtoupper($jenkel),
                        'kode_pegawai'  	=> strtoupper($kode_pegawai),
                        'status_smart_card'	=> 0
                    );
                }
            }
            // print_r($data);
            $this->DBase->insert_data_batch('tb_pegawai', $data); 
            redirect('a/p/data');
        }

    }
	
	function download($filename = NULL){
		$data = file_get_contents(base_url("assets/template_excel/".$filename));
		force_download($filename, $data);
	}
}
