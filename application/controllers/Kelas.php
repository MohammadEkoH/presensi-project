<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas extends CI_Controller { 

	public function __construct()
    {
        parent::__construct();  
        if (!$this->ion_auth->logged_in()) {//cek login ga?
    		redirect('auth','refresh');
    	}else{
            if (!$this->ion_auth->in_group('admin')) {//cek admin ga?
                redirect('auth','refresh');
            }
        }
	}
	
	public function viewAddKelas()
	{ 
        if ($this->input->get('notif') == 'a1') {
            $data['notif_gagal'] = "* Please fill in the form";
        }else{
            $data['notif_gagal'] = "";
        }

		$this->template->admin('admin/settings/siswa/vadd_kelas', $data);
	} 

	public function addkelas()
	{
		$this->form_validation->set_rules('kode_kelas', 'Kode Kelas', 'required');
		$this->form_validation->set_rules('nama_jurusan', 'Nama Jurusan', 'required'); 

		$kode_kelas 	= $this->input->post('kode_kelas');
		$nama_jurusan 	= $this->input->post('nama_jurusan'); 

		$data = array(
			'kode_kelas' => $kode_kelas, 
			'nama_jurusan' => $nama_jurusan 
		); 

		if ($this->form_validation->run() == FALSE)
		{  
			redirect('s/k/insert?notif=a1');
		}
		else
		{ 
			$this->DBase->insert_data('tb_kelas', $data);
			
			redirect('s/siswa?notif=a1');
		}
	}  

	public function viewUpdateKelas()
	{ 
		$kode_kelas = $this->input->get('q');
		$where = array('kode_kelas' => $kode_kelas);
		$data['DATA'] = $this->DBase->get_where_data('tb_kelas', $where)->row(); 

        if ($this->input->get('notif') == 'a2') {
            $data['notif_gagal'] = "* Please fill in the form";
        }else{
            $data['notif_gagal'] = "";
        }

		$this->template->admin('admin/settings/siswa/vupdate_kelas', $data);
	} 

	public function updatekelas()
	{
		$this->form_validation->set_rules('kode_kelas', 'Kode kelas', 'required');
		$this->form_validation->set_rules('nama_jurusan', 'Nama kelas', 'required'); 

		$kode_kelas 	= $this->input->post('kode_kelas');
		$nama_jurusan 	= $this->input->post('nama_jurusan'); 

		$where = array('kode_kelas' => $kode_kelas);

		$data = array( 
			'nama_jurusan' => $nama_jurusan, 
		); 

		if ($this->form_validation->run() == FALSE)
		{  
			redirect('s/k/update?notif=a2');
		}
		else
		{ 
			$this->DBase->update_data('tb_kelas', $where, $data);
			redirect('s/siswa?notif=a2');
		}
	}   

	public function deleteKelas()
	{ 
		$kode_kelas = $this->input->get('q');
		$where = array('kode_kelas' => $kode_kelas); 

		$this->DBase->delete_data('tb_kelas', $where);
		redirect('s/siswa?notif=a3');
	} 
}
