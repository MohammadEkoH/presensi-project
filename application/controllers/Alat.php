<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alat extends CI_Controller { 

	public function __construct()
    {
        parent::__construct();  
        if (!$this->ion_auth->logged_in()) {//cek login ga?
    		redirect('auth','refresh');
    	}else{
            if (!$this->ion_auth->in_group('admin')) {//cek admin ga?
                redirect('auth','refresh');
            }
        }
	}
	
	public function viewAddAlat()
	{ 
        if ($this->input->get('notif') == 'a1') {
            $data['notif_gagal'] = "* Please fill in the form";
        }else{
            $data['notif_gagal'] = "";
        }

		$this->template->admin('admin/settings/alat/vadd_alat', $data);
	} 

	public function addAlat()
	{  
		$this->form_validation->set_rules('kode_alat', 'Kode Alat', 'required');
		$this->form_validation->set_rules('nama_alat', 'Nama Alat', 'required'); 
 
		$kode_alat 	= $this->input->post('kode_alat');
		$nama_alat 	= $this->input->post('nama_alat'); 
		$kode_token	= md5(random_string('alnum', 20)); 

		$data = array(
			'kode_alat' => $kode_alat, 
			'nama_alat' => $nama_alat,
			'kode_token' => $kode_token,
			'status_alat' => 0
		); 

		if ($this->form_validation->run() == FALSE)
		{  
			redirect('s/a/insert?notif=a1');
		}
		else
		{ 
			$this->DBase->insert_data('tb_alat', $data);
			
			redirect('s/alat?notif=a1');
		}
	}  

	public function viewUpdateAlat()
	{ 
		$kode_alat = $this->input->get('q');
		$where = array('kode_alat' => $kode_alat);
		$data['DATA'] = $this->DBase->get_where_data('tb_alat', $where)->row(); 

		$this->template->admin('admin/settings/alat/vupdate_alat', $data);
	} 

	public function updateAlat()
	{
		$this->form_validation->set_rules('kode_alat', 'Kode Alat', 'required');
		$this->form_validation->set_rules('nama_alat', 'Nama Alat', 'required'); 

		$kode_alat 	= $this->input->post('kode_alat');
		$nama_alat 	= $this->input->post('nama_alat'); 

		$where = array('kode_alat' => $kode_alat);

		$data = array( 
			'nama_alat' => $nama_alat, 
		); 

		if ($this->form_validation->run() == FALSE)
		{ 
			$data['notif'] = "Please fill in the form";
			redirect('s/alat?notif=a2');
		}
		else
		{ 
			$this->DBase->update_data('tb_alat', $where, $data);
			redirect('s/alat?notif=a2');
		}
	}   

	public function deleteAlat()
	{ 
		$kode_alat = $this->input->get('q');
		$where = array('kode_alat' => $kode_alat); 

		$this->DBase->delete_data('tb_alat', $where);
		redirect('s/alat?notif=a3');
	} 
}
