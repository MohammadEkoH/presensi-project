<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template
{
	function __construct()
	{
		$this->ci =&get_instance();
	} 

	function admin($template, $data='')
	{ 
        $data_['content'] 	= $this->ci->load->view($template, $data, TRUE); 
		// $data_['modal'] 	= $this->ci->load->view("vmodal", $data, TRUE);  

		$this->ci->load->view('admin/vtemplate', $data_);
	} 

}
?>
