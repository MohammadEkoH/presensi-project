<section class="content-header">
  <h1>
    EDIT KELAS
  </h1>
  <ol class="breadcrumb">
  <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#">Settings</a></li>
    <li><a href="#">Siswa</a></li>
    <li class="active">Edit Kelas</li>
  </ol>
</section>
<section class="content">
    <span style="color:red"><?=$notif_gagal?></span>
    <div class="row"> 
        <div class="col-md-12"> 
            <div class="box box-primary"> 
                <form role="form" action="<?=base_url('s/k/update-now')?>" method="POST">
                    <div class="box-body"> 
                    <div class="form-group">
                        <label for="exampleInputPassword1">KODE KELAS</label>
                        <input type="hidden" name="kode_kelas" value="<?= $DATA->kode_kelas ?>">
                        <input disabled class="form-control" value="<?= $DATA->kode_kelas ?>" id="exampleInputPassword1" placeholder="Kode">
                    </div>   
                    <div class="form-group">
                        <label for="exampleInputEmail1">NAMA JURUSAN</label>
                        <input type="text" name="nama_jurusan" class="form-control" value="<?= $DATA->nama_jurusan ?>" id="exampleInputEmail1" placeholder="Nama">
                    </div> 
                    <div class="form-group">
                        <label for="exampleInputEmail1">JADWAL</label>
                        <select class="form-control" name="kode_jadwal" id="daterange-btn" class="select2-selection__rendered">
                            <option value="">SELECT</option>
                            <option value="PAGI">PAGI</option>
                            <option value="SIANG">SIANG</option>
                        </select>
                    </div>  
                    </div>  
                        <div class="box-footer">
                        <button type="submit" class="form-control btn btn-success">Update</button>
                        <br><br>  
                        <a href="<?= base_url('s/k/delete?q='. $DATA->kode_kelas) ?>" type="submit" class="form-control btn btn-danger">Delete</a>
                    </div>
                </form>
            </div> 
        </div> 
    </div> 
</section>