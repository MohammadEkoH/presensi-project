
<section class="content-header">
  <h1>
    EDIT TAHUN AJARAN
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#">Settings</a></li>
    <li><a href="#">Siswa</a></li>
    <li class="active">Edit Tahun Ajaran</li>
  </ol>
</section>
<section class="content">
    <div class="row"> 
        <div class="col-md-12"> 
            <div class="box box-primary"> 
                <form role="form" action="<?=base_url('s/t/update-now')?>" method="POST">
                    <div class="box-body"> 
                    <div class="form-group">
                        <label for="exampleInputPassword1">TAHUN AJARAN</label> 
                        <input disabled class="form-control" value="<?= $DATA->tahun_ajaran ?>" id="exampleInputPassword1" placeholder="Kode">
                    </div>   
                    </div>  
                        <div class="box-footer"> 
                        <a href="<?= base_url('s/t/delete?q='. $DATA->tahun_ajaran) ?>" type="submit" class="form-control btn btn-danger">Delete</a>
                    </div>
                </form>
            </div> 
        </div> 
    </div> 
</section>