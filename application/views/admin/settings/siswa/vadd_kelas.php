<section class="content-header">
  <h1>
    INSERT KELAS
  </h1>
  <ol class="breadcrumb">
  <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#">Settings</a></li>
    <li><a href="#">Siswa</a></li>
    <li class="active">Insert Kelas</li>
  </ol>
</section>
<section class="content">
    <span style="color:red">  <?=$notif_gagal?> </span>
    <div class="row"> 
        <div class="col-md-12"> 
            <div class="box box-primary"> 
                <form role="form" action="<?=base_url('s/k/insert-now')?>" method="POST">
                    <div class="box-body"> 
                    <div class="form-group">
                        <label for="exampleInputPassword1">KODE KELAS</label>
                        <input type="text" name="kode_kelas" class="form-control" id="exampleInputPassword1" placeholder="Kode Kelas">
                    </div>  
                    <div class="form-group">
                        <label for="exampleInputPassword1">TINGKAT</label>
                        <input type="number" name="tingkat" class="form-control" id="exampleInputPassword1" placeholder="Tingkat">
                    </div>  
                    <div class="form-group">
                        <label for="exampleInputEmail1">NAMA JURUSAN</label>
                        <input type="text" name="nama_jurusan" class="form-control" id="exampleInputEmail1" placeholder="Nama">
                    </div> 
                    <div class="form-group">
                        <label for="exampleInputEmail1">JADWAL</label>
                        <select class="form-control" name="kode_jadwal" id="daterange-btn" class="select2-selection__rendered">
                            <option>SELECT</option>
                            <option value="PAGI">PAGI</option>
                            <option value="SIANG">SIANG</option>
                        </select>
                    </div>  
                    </div>  
                        <div class="box-footer">
                        <button type="submit" class="form-control btn btn-primary">Insert</button>
                    </div>
                </form>
            </div> 
        </div> 
    </div> 
</section>