<section class="content-header">
  <h1>
    SETTINGS SISWA
  </h1>
  <ol class="breadcrumb">
  <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#">Settings</a></li>
    <li><a href="#">Siswa</a></li> 
  </ol>
</section> 

<section class="content">  
    <?=$notif_berhasil?>  
    <div class="row">
        <div class="col-md-6">
            <div class="box box-default">
                <div class="box-header with-border">
                <h3 class="box-title"> <i class="fa fa-cog"></i> KELAS</h3>

                <!-- <a href="<?= base_url('s/k/insert') ?>" class="btn bg-blue btn-flat margin pull-right">Add Kelas</a>   -->
                <u class="dropdown pull-right">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                       <span class="fa fa-gear" style="color:black"></span>  
                    </a>
                    <ul class="dropdown-menu">
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="<?= base_url('s/k/insert') ?>">Add Data</a></li>
                     </ul>
                </u>
                </div> 
                <div class="box-body"> 
                    <div class="direct-chat-messages">
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover"> 
                        <tr>
                            <th>NO. </th>
                            <th>KODE KELAS</th>
                            <th>NAMA JURUSAN </th>
                            <th>JADWAL </th>
                            <th> <center>ACTION</center> </th>
                        </tr>
                        <?php $no=0; 
                        foreach ($DATA_KELAS as $row) {
                            $no++; ?>
                        <tr>
                            <td><?= $no;?></td>
                            <td><?=$row->kode_kelas?></td>
                            <td><?=$row->nama_jurusan?></td>
                            <td><?=$row->kode_jadwal?></td>
                            <td align="center">
                                <a href="<?= base_url('s/k/update?q='.$row->kode_kelas) ?>" class="btn btn-warning btn-xs fa fa-pencil" title="edit"></a>
                            </td>  
                        </tr>
                        <?php } ?>
                        </table>
                    </div>
                    </div>
                    <!-- <?= $pagination?>   -->
                </div> 
            </div>  
        </div>
        <div class="col-md-6">
            <div class="box box-default">
                <div class="box-header with-border">
                <h3 class="box-title"> <i class="fa fa-cog"></i> JADWAL ABSEN</h3>
                
                <!-- <a disabled class="btn bg-blue btn-flat margin pull-right">Add Jadwal</a>   -->
                <u class="dropdown pull-right">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                       <span class="fa fa-gear" style="color:black"></span>  
                    </a>
                    <ul class="dropdown-menu">
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Add Data</a></li>
                     </ul>
                </u>                            
                </div> 
                <div class="box-body"> 
                    <div class="direct-chat-messages">
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover"> 
                        <tr>
                            <th>NO. </th>
                            <th>KODE. </th>
                            <th>TIME IN</th>  
                            <th> <center>ACTION</center> </th>
                        </tr>
                        <?php $no=0; 
                        foreach ($DATA_JADWAL as $row) {
                            $no++; ?>
                        <tr>
                            <td><?= $no ?></td>
                            <td><?= $row->kode_jadwal ?></td>
                            <td><?= $row->time_in ?> - <?= $row->time_in_ ?></td> 
                            <td align="center">
                                <a href="<?= base_url('s/j/update?q='.$row->kode_jadwal) ?>" class="btn btn-warning btn-xs fa fa-pencil" title="edit"></a>
                            </td> 
                        </tr>
                        <?php } ?>
                        </table>
                    </div>
                    </div>
                    <!-- <?= $pagination?>   -->
                </div> 
            </div>  
        </div>
    </div>  
    <div class="row">
        <div class="col-md-6">
            <div class="box box-default">
                <div class="box-header with-border">
                <h3 class="box-title"> <i class="fa fa-cog"></i> TAHUN AJARAN</h3>

                <!-- <a href="<?= base_url('s/t/insert') ?>" class="btn bg-blue btn-flat margin pull-right">Add TA</a>   -->
                <u class="dropdown pull-right">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                       <span class="fa fa-gear" style="color:black"></span>  
                    </a>
                    <ul class="dropdown-menu">
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="<?= base_url('s/t/insert') ?>">Add Data</a></li>
                     </ul>
                </u>
                </div> 
                <div class="box-body"> 
                    <div class="direct-chat-messages">
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover"> 
                        <tr>
                            <th>NO. </th>
                            <th>TAHUN AJARAN</th> 
                            <th> <center>ACTION</center> </th>
                        </tr>
                        <?php $no=0; 
                        foreach ($DATA_TA as $row) {
                            $no++; ?>
                        <tr>
                            <td><?= $no;?></td>
                            <td><?=$row->tahun_ajaran?></td> 
                            <td align="center">
                                <a href="<?= base_url('s/t/update?q='.$row->tahun_ajaran) ?>" class="btn btn-warning btn-xs fa fa-pencil" title="edit"></a>
                            </td>  
                        </tr>
                        <?php } ?>
                        </table>
                    </div>
                    </div>
                    <!-- <?= $pagination?>   -->
                </div> 
            </div>  
        </div> 
    </div>  
</section>  