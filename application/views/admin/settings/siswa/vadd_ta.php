<section class="content-header">
  <h1>
    INSERT TAHUN AJARAN
  </h1>
  <ol class="breadcrumb">
  <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#">Settings</a></li>
    <li><a href="#">Siswa</a></li>
    <li class="active">Insert Tahun Ajaran</li>
  </ol>
</section>
<section class="content">
    <span style="color:red">  <?=$notif_gagal?> </span>
    <div class="row"> 
        <div class="col-md-12"> 
            <div class="box box-primary"> 
                <form role="form" action="<?=base_url('s/t/insert-now')?>" method="POST">
                    <div class="box-body"> 
                    <div class="form-group">
                        <label for="exampleInputPassword1">TAHUN AJARAN</label>
                        <input type="text" name="ta" class="form-control" id="exampleInputPassword1" placeholder="Contoh: 2019-2020">
                    </div>   
                    </div>  
                        <div class="box-footer">
                        <button type="submit" class="form-control btn btn-primary">Insert</button>
                    </div>
                </form>
            </div> 
        </div> 
    </div> 
</section>