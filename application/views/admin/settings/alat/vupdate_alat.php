<section class="content-header">
  <h1>
    EDIT ALAT
  </h1>
  <ol class="breadcrumb">
   <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#">Settings</a></li>
    <li><a href="#">Alat</a></li>
    <li class="active">Edit Alat</li>
  </ol>
</section>
<section class="content">
    <div class="row"> 
        <div class="col-md-12"> 
            <div class="box box-primary"> 
                <form role="form" action="<?=base_url('s/a/update-now')?>" method="POST">
                    <div class="box-body"> 
                    <div class="form-group">
                        <label for="exampleInputPassword1">KODE</label>
                        <input type="hidden" name="kode_alat" value="<?= $DATA->kode_alat ?>">
                        <input disabled class="form-control" value="<?= $DATA->kode_alat ?>" id="exampleInputPassword1" placeholder="Kode">
                    </div>  
                    <div class="form-group">
                        <label for="exampleInputEmail1">NAMA</label>
                        <input type="text" name="nama_alat" class="form-control" value="<?= $DATA->nama_alat ?>" id="exampleInputEmail1" placeholder="Nama">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">URL : <a type="button" onclick="copyClipboard()">Copy</a> </label>
                        <input class="form-control" id="url" value="<?=base_url()?>store-siswa?token=<?= $DATA->kode_token ?>&uid=(id card)&kode_jadwal=(kode jadwal)" id="exampleInputPassword1" placeholder="Url">
                    </div> 
                    </div>  
                        <div class="box-footer">
                        <button type="submit" class="form-control btn btn-success">Update</button>
                        <br><br>  
                        <a href="<?= base_url('s/a/delete?q='. $DATA->kode_alat) ?>" type="submit" class="form-control btn btn-danger">Delete</a>
                    </div>
                </form>
            </div> 
        </div> 
    </div> 
</section>

<script>
    function copyClipboard() {
    /* Get the text field */
    var copyText = document.getElementById("url");

    /* Select the text field */
    copyText.select();

    /* Copy the text inside the text field */
    document.execCommand("copy");

    /* Alert the copied text */
    alert("Copied the URL: " + copyText.value);
    } 
</script>