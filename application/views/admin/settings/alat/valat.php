<section class="content-header">
  <h1>
    ALAT
  </h1>
  <ol class="breadcrumb">
   <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#">Settings</a></li> 
    <li class="active">Alat</li>
  </ol>
</section> 

<section class="content">  
  <?=$notif_berhasil?>  
    <div class="box box-default">
        <div class="box-header with-border">
        <h3 class="box-title"> <i class="fa fa-cog"></i> ALAT</h3>

        <!-- <a href="<?= base_url('s/a/insert') ?>" class="btn bg-blue btn-flat margin pull-right">Add Alat</a>   -->
        <u class="dropdown pull-right">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <span class="fa fa-gear" style="color:black"></span>  
            </a>
            <ul class="dropdown-menu">
                <li role="presentation"><a role="menuitem" tabindex="-1" href="<?= base_url('s/a/insert') ?>">Add Data</a></li>
              </ul>
        </u>
        </div> 
        <div class="box-body"> 
            <div class=" direct-chat-messages">
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover"> 
                <tr>
                    <th>NO. </th>
                    <th>KODE ALAT</th>
                    <th>NAMA ALAT</th>
                    <th>TOKEN</th>
                    <th> <center>ACTION</center> </th>
                </tr>
                <?php $no=0; 
                foreach ($DATA_ALAT as $row) {
                    $no++; ?>
                <tr>
                    <td><?= $no;?></td>
                    <td><?=$row->kode_alat?></td>
                    <td><?=$row->nama_alat?></td>
                    <td><?=$row->kode_token?></td>
                    <td align="center">
                    <a href="<?= base_url('s/a/update?q='.$row->kode_alat) ?>" class="btn btn-warning btn-xs fa fa-pencil" title="edit"></a>
                    </td>  
                </tr>
                <?php } ?>
                </table>
            </div>
            </div>
            <!-- <?= $pagination?>   -->
        </div> 
    </div>  
  
</section>  