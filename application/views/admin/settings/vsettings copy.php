<section class="content-header">
  <h1>
    Top Navigation
    <small>Example 2.0</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Layout</a></li>
    <li class="active">Top Navigation</li>
  </ol>
</section> 

<section class="content">  
  <?=$notif_berhasil?>
  <div class="row">
    <div class="col-md-6">
        <div class="box box-default">
            <div class="box-header with-border">
            <h3 class="box-title"> <i class="fa fa-cog"></i> Alat</h3>

            <a href="<?= base_url('s/a/insert') ?>" class="btn bg-blue btn-flat margin pull-right">Add Alat</a>  
             
            </div> 
            <div class="box-body"> 
              <div class=" direct-chat-messages">
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover"> 
                    <tr>
                      <th>NO. </th>
                      <th>KODE ALAT</th>
                      <th>NAMA ALAT</th>
                      <th> <center>ACTION</center> </th>
                    </tr>
                    <?php $no=0; 
                    foreach ($DATA_ALAT as $row) {
                      $no++; ?>
                    <tr>
                      <td><?= $no;?></td>
                      <td><?=$row->kode_alat?></td>
                      <td><?=$row->nama_alat?></td>
                      <td align="center">
                        <a href="<?= base_url('s/a/update?q='.$row->kode_alat) ?>" class="btn btn-warning btn-xs fa fa-pencil" title="edit"></a>
                      </td>  
                    </tr>
                    <?php } ?>
                  </table>
                </div>
              </div>
              <!-- <?= $pagination?>   -->
            </div> 
        </div> 
    </div>
    <div class="col-md-6">
        <div class="box box-default">
            <div class="box-header with-border">
            <h3 class="box-title"> <i class="fa fa-cog"></i> Kelas</h3>

            <a href="<?= base_url('s/k/insert') ?>" class="btn bg-blue btn-flat margin pull-right">Add Kelas</a>  
            
            </div> 
            <div class="box-body"> 
              <div class=" direct-chat-messages">
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover"> 
                    <tr>
                      <th>NO. </th>
                      <th>KODE KELAS</th>
                      <th>NAMA JURUSAN </th>
                      <th> <center>ACTION</center> </th>
                    </tr>
                    <?php $no=0; 
                    foreach ($DATA_KELAS as $row) {
                      $no++; ?>
                    <tr>
                      <td><?= $no;?></td>
                      <td><?=$row->kode_kelas?></td>
                      <td><?=$row->nama_jurusan?></td>
                      <td align="center">
                        <a href="<?= base_url('s/k/update?q='.$row->kode_kelas) ?>" class="btn btn-warning btn-xs fa fa-pencil" title="edit"></a>
                      </td>  
                    </tr>
                    <?php } ?>
                  </table>
                </div>
              </div>
              <!-- <?= $pagination?>   -->
            </div> 
        </div> 
    </div>
  </div>
  <div class="row">
    <div class="col-md-6">
      <div class="box box-default">
          <div class="box-header with-border">
          <h3 class="box-title"> <i class="fa fa-cog"></i> Alat</h3>

          <a href="<?= base_url('s/a/insert') ?>" class="btn bg-blue btn-flat margin pull-right">Add Alat</a>  
          
          </div> 
          <div class="box-body"> 
            <div class=" direct-chat-messages">
              <div class="box-body table-responsive no-padding">
                <table class="table table-hover"> 
                  <tr>
                    <th>NO. </th>
                    <th>KODE ALAT</th>
                    <th>NAMA ALAT</th>
                    <th> <center>ACTION</center> </th>
                  </tr>
                  <?php $no=0; 
                  foreach ($DATA_ALAT as $row) {
                    $no++; ?>
                  <tr>
                    <td><?= $no;?></td>
                    <td><?=$row->kode_alat?></td>
                    <td><?=$row->nama_alat?></td>
                    <td align="center">
                      <a href="<?= base_url('s/a/update?q='.$row->kode_alat) ?>" class="btn btn-warning btn-xs fa fa-pencil" title="edit"></a>
                    </td>  
                  </tr>
                  <?php } ?>
                </table>
              </div>
            </div>
            <!-- <?= $pagination?>   -->
          </div> 
      </div> 
    </div>
    <div class="col-md-6">
      <div class="box box-default">
          <div class="box-header with-border">
          <h3 class="box-title"> <i class="fa fa-cog"></i> Alat</h3>

          <a href="<?= base_url('s/a/insert') ?>" class="btn bg-blue btn-flat margin pull-right">Add Alat</a>  
            
          </div> 
          <div class="box-body"> 
            <div class=" direct-chat-messages">
              <div class="box-body table-responsive no-padding">
                <table class="table table-hover"> 
                  <tr>
                    <th>NO. </th>
                    <th>KODE ALAT</th>
                    <th>NAMA ALAT</th>
                    <th> <center>ACTION</center> </th>
                  </tr>
                  <?php $no=0; 
                  foreach ($DATA_ALAT as $row) {
                    $no++; ?>
                  <tr>
                    <td><?= $no;?></td>
                    <td><?=$row->kode_alat?></td>
                    <td><?=$row->nama_alat?></td>
                    <td align="center">
                      <a href="<?= base_url('s/a/update?q='.$row->kode_alat) ?>" class="btn btn-warning btn-xs fa fa-pencil" title="edit"></a>
                    </td>  
                  </tr>
                  <?php } ?>
                </table>
              </div>
            </div>
            <!-- <?= $pagination?>   -->
          </div> 
      </div> 
    </div>
  </div> 
  
</section>  