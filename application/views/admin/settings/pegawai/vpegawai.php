<section class="content-header">
  <h1>
    PEGAWAI
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#">Settings</a></li> 
    <li class="active">Pegawai</li>
  </ol>
</section> 

<section class="content">  
    <?=$notif_berhasil?>  
    <div class="row">
        <div class="col-md-6">
            <div class="box box-default">
                <div class="box-header with-border">
                <h3 class="box-title"> <i class="fa fa-cog"></i> JABATAN</h3>

                <!-- <a href="<?= base_url('s/jb/insert') ?>" class="btn bg-blue btn-flat margin pull-right">Add Jabatan</a>   -->
                <u class="dropdown pull-right">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                       <span class="fa fa-gear" style="color:black"></span>  
                    </a>
                    <ul class="dropdown-menu">
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="<?= base_url('s/jb/insert') ?>">Add Data</a></li>
                     </ul>
                </u>
                </div> 
                <div class="box-body"> 
                    <div class="direct-chat-messages">
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover"> 
                        <tr>
                            <th>NO. </th>
                            <th>KODE</th>
                            <th>JABATAN</th>
                            <th> <center>ACTION</center> </th>
                        </tr>
                        <?php $no=0; 
                        foreach ($DATA_JABATAN as $row) {
                            $no++; ?>
                        <tr>
                            <td><?= $no;?></td>
                            <td><?=$row->kode_pegawai?></td>
                            <td><?=$row->jabatan_pegawai?></td>
                            <td align="center">
                                <a href="<?= base_url('s/jb/update?q='.$row->kode_pegawai) ?>" class="btn btn-warning btn-xs fa fa-pencil" title="edit"></a>
                            </td>  
                        </tr>
                        <?php } ?>
                        </table>
                    </div>
                    </div>
                    <!-- <?= $pagination?>   -->
                </div> 
            </div>  
        </div>
        <div class="col-md-6">
            <div class="box box-default">
                <div class="box-header with-border">
                <h3 class="box-title"> <i class="fa fa-cog"></i> JADWAL ABSEN</h3>
  
                <u class="dropdown pull-right">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                       <span class="fa fa-gear" style="color:black"></span>  
                    </a>
                    <ul class="dropdown-menu">
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Add Data</a></li>
                     </ul>
                </u>
                </div> 
                <div class="box-body"> 
                    <div class="direct-chat-messages">
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover"> 
                        <tr>
                            <th>NO. </th>
                            <th>KODE. </th>
                            <th>TIME IN</th> 
                            <th>TIME OUT</th> 
                            <th> <center>ACTION</center> </th>
                        </tr>
                        <?php $no=0; 
                        foreach ($DATA_JADWAL as $row) {
                            $no++; ?>
                        <tr>
                            <td><?= $no ?></td>
                            <td><?= $row->kode_jadwal ?></td>
                            <td><?= $row->time_in ?> - <?= $row->time_in_ ?></td>
                            <td><?= $row->time_out ?> - <?= $row->time_out_ ?></td>
                            <td align="center">
                                <a href="<?= base_url('s/jp/update?q='.$row->kode_jadwal) ?>" class="btn btn-warning btn-xs fa fa-pencil" title="edit"></a>
                            </td> 
                        </tr>
                        <?php } ?>
                        </table>
                    </div>
                    </div>
                    <!-- <?= $pagination?>   -->
                </div> 
            </div>  
        </div>
    </div>  
</section>  