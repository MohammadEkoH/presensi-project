<section class="content-header">
  <h1>
    EDIT JABATAN
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#">Settings</a></li>
    <li><a href="#">Pegawai</a></li>
    <li class="active">Edit Jabatan</li>
  </ol>
</section>
<section class="content">
    <div class="row"> 
        <div class="col-md-12"> 
            <div class="box box-primary"> 
                <form role="form" action="<?=base_url('s/jb/update-now')?>" method="POST">
                    <div class="box-body"> 
                    <div class="form-group">
                        <label for="exampleInputPassword1">KODE PEGAWAI</label>
                        <input type="hidden" name="kode_pegawai" value="<?= $DATA->kode_pegawai ?>">
                        <input disabled class="form-control" value="<?= $DATA->kode_pegawai ?>" id="exampleInputPassword1" placeholder="Kode">
                    </div>  
                    <div class="form-group">
                        <label for="exampleInputEmail1">JABATAN PEGAWAI</label>
                        <input type="text" name="jabatan_pegawai" class="form-control" value="<?= $DATA->jabatan_pegawai ?>" id="exampleInputEmail1" placeholder="Jabatan">
                    </div>
                    </div>  
                        <div class="box-footer">
                        <button type="submit" class="form-control btn btn-success">Update</button>
                        <br><br>  
                        <a href="<?= base_url('s/jb/delete?q='. $DATA->kode_pegawai) ?>" type="submit" class="form-control btn btn-danger">Delete</a>
                    </div>
                </form>
            </div> 
        </div> 
    </div> 
</section>