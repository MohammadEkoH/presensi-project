<section class="content-header">
  <h1>
    EDIT JADWAL
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#">Settings</a></li>
    <li><a href="#">Pegawai</a></li>
    <li class="active">Edit Jadwal</li>
  </ol>
</section>
<section class="content">
    <div class="row"> 
        <div class="col-md-12"> 
            <div class="box box-primary"> 
                <form role="form" action="<?=base_url('s/jp/update-now')?>" method="POST">
                    <div class="box-body"> 
                    <div class="form-group">
                        <label for="exampleInputPassword1">KODE JADWAL</label>
                        <input type="hidden" name="kode_jadwal" value="<?= $DATA->kode_jadwal ?>">
                        <input disabled class="form-control" value="<?= $DATA->kode_jadwal ?>" id="exampleInputPassword1" placeholder="Kode">
                    </div>  
                    <div class="form-group">
                        <label for="exampleInputEmail1">TIME IN</label>
                        <input type="time" name="time_in" class="form-control" value="<?= $DATA->time_in ?>" id="exampleInputEmail1" placeholder="Time">
                        <input type="time" name="time_in_" class="form-control" value="<?= $DATA->time_in_ ?>" id="exampleInputEmail1" placeholder="Time">
                    </div> 
                    <div class="form-group">
                        <label for="exampleInputEmail1">TIME OUT</label>
                        <input type="time" name="time_out" class="form-control" value="<?= $DATA->time_out ?>" id="exampleInputEmail1" placeholder="Time">
                        <input type="time" name="time_out_" class="form-control" value="<?= $DATA->time_out_ ?>" id="exampleInputEmail1" placeholder="Time">
                    </div>
                    </div>  
                        <div class="box-footer">
                        <button type="submit" class="form-control btn btn-success">Update</button>
                        <br><br>  
                    </div>
                </form>
            </div> 
        </div> 
    </div> 
</section>