<section class="content-header">
  <h1>
    INSERT JABATAN
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#">Settings</a></li>
    <li><a href="#">Pegawai</a></li>
    <li class="active">Insert Jabatan</li>
  </ol>
</section>
<section class="content">
    <span style="color:red">  <?=$notif_gagal?> </span>
    <div class="row"> 
        <div class="col-md-12"> 
            <div class="box box-primary">  
                <form role="form" action="<?=base_url('s/jb/insert-now')?>" method="POST">
                    <div class="box-body"> 
                    <div class="form-group">
                        <label for="exampleInputPassword1">KODE PEGAWAI</label>
                        <input type="text" name="kode_pegawai" class="form-control" id="exampleInputPassword1" placeholder="Kode">
                    </div>  
                    <div class="form-group">
                        <label for="exampleInputEmail1">JABATAN PEGAWAI</label>
                        <input type="text" name="jabatan_pegawai" class="form-control" id="exampleInputEmail1" placeholder="Jabatan">
                    </div>
                    </div>  
                        <div class="box-footer">
                        <button type="submit" class="form-control btn btn-primary">Insert</button>
                    </div>
                </form>
            </div> 
        </div> 
    </div> 
</section>