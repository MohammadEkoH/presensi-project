<section class="content-header">
  <h1>
    PROFILE
  </h1>
  <ol class="breadcrumb">
  <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li> 
    <li >Settings</li>
    <li class="active">Profile</li>
  </ol>
</section>
<section class="content">
    <!-- <span style="color:red">  <?=$massege?> </span>  -->
        <!-- Custom Tabs --> 
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab">Biodata Diri</a></li>  
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1">  
                Username: <?= $data_users->username ?><br>
                Firs Name: <?= $data_users->first_name ?><br>
                Last Name: <?= $data_users->last_name ?><br>
                Email: <?= $data_users->email ?><br>
                Phone: <?= $data_users->phone ?>  
                <br><br><br>
                <a href="<?=base_url('auth/edit_user/'.$data_users->id)?>" class="form-control btn btn-success">EDIT</a>
            </div> 
        </div> 
    </div>  
</section>

