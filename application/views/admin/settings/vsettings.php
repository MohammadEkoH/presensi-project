<section class="content-header">
  <h1>
    SETTINGS 
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li> 
    <li class="active">Settings</li>
  </ol>
</section> 

<section class="content">  
  <?=$notif_berhasil?>  
  <div class='alert alert-info alert-dismissible'>
    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
    <i style="color:black">
      Selamat datang dihalaman settings!
    </i>
  </div>  
  <div class="form-group">
    <a href="<?=base_url('s/profile')?>" class="form-control btn btn-primary"> PROFILE</a>
  </div>
  <div class="form-group">
    <a href="<?=base_url('s/alat')?>" class="form-control btn btn-primary"> ALAT</a>
  </div> 
  <div class="form-group">
    <a href="<?=base_url('s/pegawai')?>" class="form-control btn btn-primary"> PEGAWAI</a>
  </div> 
  <div class="form-group">
    <a href="<?=base_url('s/siswa')?>" class="form-control btn btn-primary"> SISWA</a>
  </div>  
</section>  