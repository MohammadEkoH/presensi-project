<section class="content-header">
  <h1>
    REPORT HARIAN PEGAWAI
  </h1>
  <ol class="breadcrumb">
   <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li> 
    <li class="active">Report harian Pegawai</li>
  </ol>
</section>
 
<section class="content">   
  <form  class="form-inline" action="<?=base_url('a/p/report-harian')?>" method="get">
    <!-- <select value="<?=$val_kode_pegawai?>" class="" id="kode-pegawai" name="kode_pegawai" id="daterange-btn val-name-siswa" class="">
      <option value="">Jabatan</option>
      <?php foreach ($data_jabatan as $row) { ?>
        <option value="<?=$row->kode_pegawai?>"><?=$row->jabatan_pegawai?></option>
      <?php } ?>
    </select>  -->
     
    <!-- <input type="date" id="tanggal-pegawai" name="tanggal" id="val-date-siswa" class="select2-selection__rendered" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask> 
       
    <button type="submit" class="btn bg-blue btn-flat margin"> <i class="fa fa-search"></i> </button> -->
      
    <div class="form-group">
      <div class="input-group">
        <input type="date" id="tanggal-pegawai" name="tanggal" id="val-date-siswa" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
        <span class="input-group-btn">
          <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i> </button>
        </span>
      </div> 
    </div>     

    <a href="<?=base_url('a/p/report-harian-print?'.'tanggal='.$val_tanggal_pegawai)?>" title="print" id="btn-print-harian-pegawai" class="btn bg-blue btn-flat margin pull-right"  style="display:">Print</a>  
  </form>  
   <br>
  <div class="box">  
    <?php if ($val_tanggal_pegawai) : ?>
      <div class="box-body table-responsive no-padding direct-chat-messages">
        <table class="table table-hover"> 
          <tr>
              <th>NO.</th>
              <th>NO. INDUK</th> 
              <th>NAMA</th>  
              <th>JABATAN</th>  
              <th>JAM MASUK</th> 
              <th>STATUS MASUK</th> 
              <th>JAM KELUAR</th> 
              <th>STATUS KELUAR</th> 
              <th>KETERLAMBATAN</th> 
              <th>TANGGAL</th>  
          </tr>  
          <?php $no=0;
            foreach ($DATA->result() as $row) {
            $no++;
          ?> 
          <tr>
            <td><?= $no?></td>  
            <td><?= $row->no_induk_pegawai?></td>
            <td><?= $row->nama_pegawai?></td>  
            <td><?= $row->jabatan_pegawai?></td>  
            <td><?= $row->jam_masuk?></td> 
            <td><?= $row->status_masuk?></td>
            <td><?= $row->jam_keluar?></td> 
            <td><?= $row->status_keluar?></td>
            <td><?= $row->keterlambatan?></td> 
            <td><?= mediumdate_indo($row->tanggal)?></td> 
          </tr>
          <?php } ?>
        </table>
      </div>
      <div class="box-footer clearfix">
        <ul class="pagination pagination-sm no-margin pull-left">
          <b>TOTAL PEGAWAI</b> : <?=$DATA->num_rows()?>
        </ul> 
      </div>
    <?php else: ?>
      <div class="box-footer clearfix">
        <ul class="pagination pagination-sm no-margin pull-left">
          <b>TOTAL PEGAWAI</b> : <?=$DATA->num_rows()?>
        </ul> 
        <div align="right">
              <b style="color:red">*</b> <b>Masukkan Hari/Bulan/Tahun</b>
        </div>
      </div>
    <?php endif;?> 
  </div>
</section> 
 