<section class="content-header">
  <h1>
    INSERT DATA PEGAWAI
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li> 
    <li><a href="#">Pegawai</a></li>
    <li class="active">Insert Data Pegawai</li>
  </ol>
</section>
<section class="content">
<span style="color:red">  <?=$notif_gagal?> </span> 
    <div class="row"> 
        <div class="col-md-12"> 
            <div class="box box-primary"> 
                <form role="form" action="<?=base_url()?>pegawai/addDataPegawai" method="POST">
                    <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">NO. INDUK</label>
                        <input type="number" name="no_induk" class="form-control" id="exampleInputEmail1" placeholder="No. Induk">
                    </div> 
                    <div class="form-group">
                        <label for="exampleInputPassword1">NAMA</label>
                        <input type="text" name="nama" class="form-control" id="exampleInputPassword1" placeholder="Nama">
                    </div> 
                    <div class="form-group">
                        <label for="exampleInputPassword1">EMAIL</label>
                        <input type="text" name="email" class="form-control" id="exampleInputPassword1" placeholder="Email">
                    </div> 
                    <div class="form-group">
                        <label for="exampleInputPassword1">JENIS KELAMIN</label>
                        <select name="jenkel" class="form-control" id="">
                            <option value="L">Laki-Laki</option>
                            <option value="P">Perempuan</option>
                        </select>
                    </div>   
                    <div class="form-group">
                    <label for="exampleInputPassword1">JABATAN</label>
                    <select class="form-control" name="jabatan" id="daterange-btn" class="select2-selection__rendered">
                        <option>SELECT</option>
                        <?php
                        $no = 1;
                        foreach ($data_jabatan as $row) { ?>  
                            <option value="<?=$row->kode_pegawai?>"> <?=$row->jabatan_pegawai?> </option>
                        <?php } ?>
                        </select>
                    </div>    
                    </div>  
                        <div class="box-footer">
                        <button type="submit" class="form-control btn btn-primary">Insert</button>
                    </div> 
                </form>
            </div> 
        </div> 
    </div> 
</section>