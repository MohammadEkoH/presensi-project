<section class="content-header">
  <h1>
    REPORT BULANAN PEGAWAI
  </h1>
  <ol class="breadcrumb">
   <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li> 
    <li class="active">Report Bulanan Pegawai</li>
  </ol>
</section>
 
<section class="content">   
  <form class="form-inline" action="<?=base_url('a/p/report-bulanan')?>" method="get">

    <div class="form-group">
      <div class="input-group">
        <input type="text" name="bulan" class="form-control" id="mydate" placeholder="bulan-tahun" readonly require>
        <span class="input-group-btn">
          <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i> </button>
        </span>
      </div>
    </div>

    <a href="<?=base_url('a/p/report-bulanan-print?'.'tanggal='.$val_tanggal_pegawai)?>" title="print" id="btn-print-harian-pegawai" class="btn bg-blue btn-flat margin pull-right"  style="display:">Print</a>  
    
    <script>
      $("#mydate").datepicker( {
        format: "mm-yyyy",
        viewMode: "months", 
        minViewMode: "months"
      });
    </script>

  </form>  
  <br>
  <div class="box"> 
    <!-- /.box-header -->
    <?php if (count($data_absensi)): ?>
      <div class="box-body table-responsive no-padding  direct-chat-messages">
        <table class="table table-hover"> 
          <tr>
            <th rowspan="2" class="text-center">NO. </th>
            <th rowspan="2" class="text-center">NO. INDUK</th>  
            <th rowspan="2" class="text-center">NAMA</th>  
            <th rowspan="2" class="text-center">JABATAN</th>
            <th colspan="<?=$count_days?>" class="text-center"><?=bulan($val_month).' '.$val_years?></th>  
          </tr>
          <tr> 
            <?php for($i=1; $i<=$count_days; $i++){ ?>
                <th> <?= $i; ?> </th>
            <?php }?>
          </tr> 
          <?php $no = 1; ?>
          <?php foreach ($data_absensi as $pegawai) : ?>
            <tr>
              <td><?=$no?></td>
              <td><?=$pegawai['no_induk']?></td>
              <td><?=$pegawai['name']?></td>
              <td><?=$pegawai['jabatan']?></td>
              <?php foreach ($pegawai['absensi'] as $absensi): ?>
                <?php if ($absensi['absen']): ?>
                  <td><i class="fa fa-check" style="color:green"></i></td>
                <?php else: ?>
                  <td><i class="fa fa-close" style="color:red"></i></td>
                <?php endif; ?>
              <?php endforeach; ?>
            </tr>
            <?php $no++; ?>
          <?php endforeach; ?> 
        </table>
      </div>
      <div class="box-footer clearfix">
        <ul class="pagination pagination-sm no-margin pull-left">
          <b>TOTAL PEGAWAI</b> :  <?=count($data_absensi) ?>
        </ul> 
      </div>
    
    <?php else: ?>
      <div class="box-footer clearfix">
        <ul class="pagination pagination-sm no-margin pull-left">
          <b>TOTAL PEGAWAI</b> :  <?=count($data_absensi) ?>
        </ul> 
        <div align="right">
              <b style="color:red">*</b> <b>Masukkan Bulan/Tahun</b>
        </div>
      </div> 
    <?php endif; ?>
  </div>
</section> 