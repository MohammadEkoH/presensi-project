<section class="content-header">
  <h1>
    DATA PEGAWAI 
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>  
    <li class="active">Data Pegawai</li>
  </ol>
</section>
 
<section class="content">   
  <?=$notif_berhasil?>
  <form action="<?=base_url()?>a/p/search-data" method="POST"> 
    <select class="" name="jabatan" id="daterange-btn" class="select2-selection__rendered">
      <option>JABATAN</option>
      <?php
      $no = 1;
      foreach ($data_jabatan as $row) { ?>  
        <option value="<?=$row->kode_pegawai?>"> <?=$row->jabatan_pegawai?> </option>
      <?php } ?>
    </select> 

    <input type="text" name="nama" placeholder="Cari Pegawai">

    <button type="submit" class="btn bg-blue btn-flat margin" title="search"> <i class="fa fa-search"></i> </button>
    <a href="<?= base_url() ?>a/p/insert-data" class="btn bg-blue btn-flat margin pull-right">Add</a>    
    <a href="<?= base_url() ?>a/p/import-data" class="btn bg-blue btn-flat margin pull-right">Import </a>    
    <div class="box"> 
      <!-- /.box-header -->
      <div class="box-body table-responsive no-padding direct-chat-messages">
        <table class="table table-hover">
          <tr>
            <th>NO.</th>
            <th>NO. INDUK</th> 
            <th>NAMA</th>
            <th>EMAIL</th>
            <th>KELAMIN</th> 
            <th>JABATAN</th> 
            <th>STATUS</th>
            <th> <center>ACTION</center> </th>
          </tr>

          <?php 
              if($this->uri->segment(4)){
                  $no=$this->uri->segment(4);
              } else{
                  $no=0;
              }
              foreach ($data_pegawai->result() as $row) { 
                $no++; 
                if($row->status_smart_card == 1){?>  
                <tr>
                  <td><?=$no?></td>  
                  <td><?=$row->no_induk_pegawai?></td>  
                  <td><?=$row->nama_pegawai?></td>  
                  <td><?=$row->email_pegawai?></td>  
                  <td><?=$row->jenkel_pegawai?></td>  
                  <td><?=$row->jabatan_pegawai?></td>   
                  <td> <a href="<?= base_url('edit-status-card-pegawai?id_pegawai='.$row->id_pegawai.'&status='.$row->status_smart_card) ?>" class="btn btn-xs btn-success" title="non-active now">REGISTERED</a> </td> 
                  <td align="center">
                    <a href="<?= base_url('a/p/update-data?p=' . $row->id_pegawai) ?>" class="btn btn-warning btn-xs fa fa-pencil" title="edit"></a>
                  </td> 
                </tr>
                <?php } else { ?> 
                <tr>
                  <td><?=$no?></td>  
                  <td><?=$row->no_induk_pegawai?></td>  
                  <td><?=$row->nama_pegawai?></td>  
                  <td><?=$row->email_pegawai?></td>  
                  <td><?=$row->jenkel_pegawai?></td>  
                  <td><?=$row->jabatan_pegawai?></td>   
                  <td> <a href="<?= base_url('edit-status-card-pegawai?id_pegawai='.$row->id_pegawai.'&status='.$row->status_smart_card) ?>" class="btn btn-xs btn-danger" title="non-active now">UNREGISTED</a> </td> 
                  <td align="center">
                    <a href="<?= base_url('a/p/update-data?p=' . $row->id_pegawai) ?>" class="btn btn-warning btn-xs fa fa-pencil" title="edit"></a>
                  </td> 
                </tr>
              <?php 
                }
              } ?> 
              
        </table>
      </div> 
      <div class="box-footer clearfix">
        <ul class="pagination pagination-sm no-margin pull-left">
              <?php
                if($data_pegawai->num_rows() > 0){
                  $count;
                }else{
                  $count = 0;
                }
              ?>
          <b>TOTAL PEGAWAI</b> : <?=$count?> 
        </ul>
        <div align="right">
          <span style="color:red"> <?= $notif_limit ?> </span>
        </div>
        <?= $pagination?>  
      </div>
    </div>
  </form>
</section> 