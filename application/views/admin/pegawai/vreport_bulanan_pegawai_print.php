<table border="1"  cellspacing="0">
    <tr>
        <th rowspan="2" class="text-center">NO. </th>
        <th rowspan="2" class="text-center">NO. INDUK</th>
        <th rowspan="2" class="text-center">NAMA</th>  
        <th rowspan="2" class="text-center">JABATAN</th>
        <th colspan="<?=$count_days?>" class="text-center"><?=bulan($val_month).' '.$val_years?></th>  
    </tr>
    <tr> 
        <?php for($i=1; $i<=$count_days; $i++){ ?>
            <th> <?= $i; ?> </th>
        <?php }?>
    </tr>
    <?php $no = 1; ?>
        <?php foreach ($data_absensi as $pegawai) : ?>
            <tr>
            <td><?=$no?></td>
            <td><?=$pegawai['no_induk']?></td>
            <td><?=$pegawai['name']?></td>
            <td><?=$pegawai['jabatan']?></td>
            <?php foreach ($pegawai['absensi'] as $absensi): ?>
                <?php if ($absensi['absen']): ?>
                <td><i style="color:green">-</i></td>
                <?php else: ?>
                <td><i style="color:red">x</i></td>
                <?php endif; ?>
            <?php endforeach; ?>
            </tr>
            <?php $no++; ?>
        <?php endforeach; ?>
</table>

<script>
    window.print();
</script>