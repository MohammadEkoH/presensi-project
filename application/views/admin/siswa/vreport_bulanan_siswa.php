<section class="content-header">
  <h1>
    REPORT BULANAN SISWA
  </h1>
  <ol class="breadcrumb">
   <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li> 
    <li class="active">Report Bulanan Siswa</li>
  </ol>
</section>
 
<section class="content">   
<form class="form-inline" action="<?=base_url('a/s/report-bulanan')?>" method="get">
    <select class="form-control" name="ta" value="">
      <option value="">Tahun Ajaran</option>
      <?php foreach ($data_ta->result() as $row) { ?>  
        <option value="<?=$row->tahun_ajaran?>"> <?=$row->tahun_ajaran?> </option>
      <?php } ?>
    </select>   

    <select class="form-control" name="kelas" id="daterange-btn" class="select2-selection__rendered">
      <option value="">Kelas</option>
      <?php foreach ($data_kelas->result() as $row) { ?>  
        <option value="<?=$row->kode_kelas?>"> <?=$row->kode_kelas?> </option>
      <?php } ?>
    </select>

    <div class="form-group">
      <div class="input-group">
      <input type="text" name="bulan" class="form-control" id="mydate" placeholder="bulan-tahun" readonly require>
        <span class="input-group-btn">
          <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i> </button>
        </span>
      </div>
    </div> 
        
    <a href="<?=base_url('a/s/report-bulanan-print?ta='.$val_ta.'&kelas='.$val_kelas.'&bulan='.$val_bulan)?>" title="print"  class="btn bg-blue btn-flat margin pull-right"  style="display:">Print</a>   

    <script>
      $("#mydate").datepicker( {
        format: "mm-yyyy",
        viewMode: "months", 
        minViewMode: "months"
      });
    </script>
  </form>  
  <br>
  <div class="box"> 
    <!-- /.box-header -->
    <?php if (count($data_absensi)): ?>
      <div class="box-body table-responsive no-padding  direct-chat-messages">
        <table class="table table-hover"> 
          <thead> 
            <th rowspan="2" class="text-center">NO. </th>
            <th rowspan="2" class="text-center">NO. INDUK</th>  
            <th rowspan="2" class="text-center">NAMA</th>  
            <th rowspan="2" class="text-center">KELAS</th> 
            <th rowspan="2" class="text-center">TAHUN AJARAN</th>
            <th colspan="<?=$count_days?>" class="text-center"><?=bulan($val_month).' '.$val_years?></th>  
          </thead>
          <thead>  
            <th colspan="5">
              <?php for($i=1; $i<=$count_days; $i++){ ?>
                  <th> <?= $i; ?> </th>
              <?php }?>
            </th>
          </thead>
          <tbody>
            <?php $no = 1; ?>
            <?php foreach ($data_absensi as $siswa) : ?>
              <tr>
                <td><?=$no?></td>
                <td><?=$siswa['no_induk']?></td>
                <td><?=$siswa['name']?></td>
                <td><?=$siswa['kelas']?></td>
                <td><?=$siswa['ta']?></td> 
                <?php foreach ($siswa['absensi'] as $absensi): ?>
                  <?php if ($absensi['absen']): ?>
                    <td><i class="fa fa-check" style="color:green"></i></td>
                  <?php else: ?>
                    <td><i class="fa fa-close" style="color:red"></i></td>
                  <?php endif; ?>
                <?php endforeach; ?>
              </tr>
              <?php $no++; ?>
            <?php endforeach; ?>
              
          </tbody>
        </table>
      </div>
      <div class="box-footer clearfix">
        <ul class="pagination pagination-sm no-margin pull-left">
          <b>TOTAL SISWA</b> :  <?=count($data_absensi) ?>
        </ul> 
      </div>
    
    <?php else: ?>
      <div class="box-footer clearfix">
        <ul class="pagination pagination-sm no-margin pull-left">
          <b>TOTAL SISWA</b> :  <?=count($data_absensi) ?>
        </ul> 
        <div align="right">
          <b style="color:red">*</b> <b>Masukkan (Tahun Ajaran), (Kelas), (Hari/Bulan/Tahun)</b>
        </div>
      </div> 
    <?php endif; ?>
  </div>
</section> 