<section class="content-header">
  <h1>
    INSERT DATA SISWA
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#">Data Siswa</a></li>
    <li class="active">Insert Data Siswa</li>
  </ol>
</section>
<section class="content">
<span style="color:red">  <?=$notif_gagal?> </span>
    <div class="row"> 
        <div class="col-md-12"> 
            <div class="box box-primary"> 
                <form role="form" action="<?=base_url('siswa/addDataSiswa')?>" method="POST">
                    <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">NO. INDUK</label>
                        <input type="number" min="15" name="no_induk" class="form-control" id="exampleInputEmail1" placeholder="No. Induk">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">NISN</label>
                        <input type="number" min="15" name="nisn" class="form-control" id="exampleInputPassword1" placeholder="NISN">
                    </div> 
                    <div class="form-group">
                        <label for="exampleInputPassword1">NAMA</label>
                        <input type="text" name="nama" class="form-control" id="exampleInputPassword1" placeholder="Nama">
                    </div> 
                    <div class="form-group">
                        <label for="exampleInputPassword1">JENIS KELAMIN</label>
                        <select name="jenkel" class="form-control" id="">
                            <option value="L">Laki-Laki</option>
                            <option value="P">Perempuan</option>
                        </select>
                    </div> 
                    <div class="form-group">
                        <label for="exampleInputPassword1">TEMPAT</label>
                        <input type="text" name="tempat" class="form-control" id="exampleInputPassword1" placeholder="Tempat">
                    </div> 
                    <div class="form-group">
                        <label for="exampleInputPassword1">TANGGAL LAHIR</label>
                        <input type="date" name="ttl" class="form-control" id="exampleInputPassword1" placeholder="Tanggal Lahir">
                    </div> 
                    <div class="form-group">
                        <label for="exampleInputPassword1">ALAMAT</label>
                        <input type="text" name="alamat" class="form-control" id="exampleInputPassword1" placeholder="Alamat">
                    </div> 
                    <div class="form-group">
                        <label for="exampleInputPassword1">NO. HP</label>
                        <input type="number"  min="10" name="no_hp" class="form-control" id="exampleInputPassword1" placeholder="No. HP">
                    </div>  
                    <div class="form-group">
                        <label for="exampleInputPassword1">TAHUN AJARAN</label>
                        <select class="form-control" name="ta" id="daterange-btn" class="select2-selection__rendered">
                            <option>SELECT</option>
                            <?php
                            $no = 1;
                            foreach ($data_ta as $row) { ?>  
                                <option value="<?=$row->tahun_ajaran?>"> <?=$row->tahun_ajaran?> </option>
                            <?php } ?>
                        </select>
                    </div>  
                    <div class="form-group">
                    <label for="exampleInputPassword1">KELAS</label>
                    <select class="form-control" name="kelas" id="daterange-btn" class="select2-selection__rendered">
                        <option>SELECT</option>
                        <?php
                        $no = 1;
                        foreach ($data_kelas as $row) { ?>  
                            <option value="<?=$row->tingkat?>-<?=$row->kode_kelas?>"><?=$row->tingkat?> - <?=$row->kode_kelas?> </option>
                        <?php } ?>
                    </select>
                    </div>   
                    </div>  
                        <div class="box-footer">
                        <button type="submit" class="form-control btn btn-primary">Insert</button>
                    </div> 
                </form>
            </div> 
        </div> 
    </div> 
</section>