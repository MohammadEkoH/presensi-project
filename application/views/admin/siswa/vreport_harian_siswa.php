<section class="content-header">
  <h1>
    REPORT HARIAN SISWA
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>  
    <li class="active">Report Harian Siswa</li>
  </ol>
</section>

<section class="content">   
  <form class="form-inline" action="<?=base_url('a/s/report-harian')?>" method="get">
    <select class="form-control" name="ta" value="">
      <option value="">Tahun Ajaran</option>
      <?php foreach ($data_ta->result() as $row) { ?>  
        <option value="<?=$row->tahun_ajaran?>"> <?=$row->tahun_ajaran?> </option>
      <?php } ?>
    </select>   

    <select class="form-control" name="kode_kelas" id="daterange-btn" class="select2-selection__rendered">
      <option value="">Kelas</option>
      <?php foreach ($data_kelas->result() as $row) { ?>  
        <option value="<?=$row->kode_kelas?>"> <?=$row->kode_kelas?> </option>
      <?php } ?>
    </select>

    <div class="form-group">
      <div class="input-group">
        <input type="date" name="tanggal" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask> 
        
        <span class="input-group-btn">
          <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i> </button>
        </span>
      </div>
    </div> 
 
    <a href="<?=base_url('a/s/report-harian-print?ta='.$val_ta.'&kelas='.$val_kelas.'&tanggal='.$val_tanggal)?>" title="print"  class="btn bg-blue btn-flat margin pull-right"  style="display:">Print</a>   
  </form>  
  <br>
  <div class="box"> 
    <?php if ($val_ta && $val_kelas && $val_tanggal) : ?>
      <form class="form-update">  
        <div class="box-body table-responsive no-padding direct-chat-messages">
            <table class="table table-hover"> 
              <tr>
                  <th>NO.</th>
                  <th>NISN</th> 
                  <th>NAMA</th> 
                  <th>KELAS</th> 
                  <th>JAM MASUK</th> 
                  <th>KETERLAMBATAN</th> 
                  <th>TANGGAL</th>  
                  <th>STATUS</th>  
              </tr>  
              <?php $no=0;
              foreach ($DATA->result() as $row) {
                $no++;
              ?>
              <tr>
                  <td><?= $no?></td>  
                  <td><?= $row->nisn_siswa?></td>
                  <td><?= $row->nama_siswa?></td> 
                  <td> <?= $row->kode_kelas ?></td>
                  <td><?= $row->jam_masuk?></td> 
                  <td><?= $row->keterlambatan?></td> 
                  <td><?= mediumdate_indo($row->tanggal)?></td> 
                  <td><?= $row->status?></td>
                  <!-- <td>
                    <select name="status">
                      <option value="A">A</option>
                      <option value="H">H</option>
                      <option value="I">I</option>
                      <option value="S">S</option>
                    </select>
                  </td> -->
              </tr> 
              <?php } ?>
            </table>
        </div>  
      </form>
      <div class="box-footer clearfix">
        <ul class="pagination pagination-sm no-margin pull-left">
          <b>TOTAL SISWA</b> : <?=$DATA->num_rows() ?> 
        </ul>  
      </div>
    <?php else: ?> 
      <div class="box-footer clearfix">
        <ul class="pagination pagination-sm no-margin pull-left">
          <b>TOTAL SISWA</b> : <?=$DATA->num_rows() ?> 
        </ul> 
        <div align="right">
          <b style="color:red">*</b> <b>Masukkan (Tahun Ajaran), (Kelas), (Hari/Bulan/Tahun)</b>
        </div> 
      </div>
    <?php endif; ?> 
  </div>
</section> 