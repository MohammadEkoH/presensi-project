<table border="1"  cellspacing="0"> 
    <tr>
        <th rowspan="2" class="text-center">NO. </th>
        <th rowspan="2" class="text-center">NO. INDUK</th>  
        <th rowspan="2" class="text-center">NAMA</th>  
        <th rowspan="2" class="text-center">KELAS</th> 
        <th rowspan="2" class="text-center">TAHUN AJARAN</th>
        <th colspan="<?=$count_days?>" class="text-center"><?=bulan($val_month).' '.$val_years?></th>  
    </tr>
    <tr> 
        <?php for($i=1; $i<=$count_days; $i++){ ?>
            <th> <?= $i; ?> </th>
        <?php }?> 
    </tr> 
    
    <!-- foreach -->
    <?php $no = 1; ?>
        <?php foreach ($data_absensi as $siswa) : ?>
            <tr>
                <td><?=$no?></td>
                <td><?=$siswa['no_induk']?></td>
                <td><?=$siswa['name']?></td>
                <td><?=$siswa['kelas']?></td>
                <td><?=$siswa['ta']?></td>
                <?php foreach ($siswa['absensi'] as $absensi): ?>
                    <?php if ($absensi['absen']): ?>
                    <td align="center"><i class="fa fa-check" style="color:green">-</i></td>
                    <?php else: ?>
                    <td align="center"><i class="fa fa-close" style="color:red">x</i></td>
                    <?php endif; ?>
                <?php endforeach; ?>
            </tr>
            <?php $no++; ?>
        <?php endforeach; ?> 
</table>

<script>
    window.print();
</script>