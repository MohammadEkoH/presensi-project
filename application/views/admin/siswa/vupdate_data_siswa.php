<section class="content-header">
  <h1>
    EDIT DATA SISWA
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#">Data Siswa</a></li>
    <li class="active">Edit Data Siswa</li>
  </ol>
</section>
<section class="content">
<span style="color:red">  <?=$notif_gagal?> </span> 
    <div class="row"> 
        <div class="col-md-12"> 
            <div class="box box-primary"> 
                <form role="form" action="<?=base_url()?>siswa/updateDataSiswa" method="POST">
                    <div class="box-body">
                    <input type="hidden" name="id" value="<?=$DATA->id_siswa?>">
                    <div class="form-group">
                        <label for="exampleInputEmail1">NO. INDUK</label>
                        <input type="number" value="<?= $DATA->no_induk_siswa ?>" name="no_induk" class="form-control" id="exampleInputEmail1" placeholder="No. Induk">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">NISN</label>
                        <input type="number" value="<?= $DATA->nisn_siswa ?>" name="nisn" class="form-control" id="exampleInputPassword1" placeholder="NISN">
                    </div> 
                    <div class="form-group">
                        <label for="exampleInputPassword1">NAMA</label>
                        <input type="text" value="<?= $DATA->nama_siswa ?>" name="nama" class="form-control" id="exampleInputPassword1" placeholder="Nama">
                    </div> 
                    <div class="form-group">
                        <label for="exampleInputPassword1">JENIS KELAMIN</label>
                        <select name="jenkel" value="<?= $DATA->jenkel_siswa ?>" class="form-control" id="">
                            <option value="L">Laki-Laki</option>
                            <option value="P">Perempuan</option>
                        </select>
                    </div> 
                    <div class="form-group">
                        <label for="exampleInputPassword1">TEMPAT</label>
                        <input type="text" value="<?= $DATA->tempat_siswa ?>" name="tempat" class="form-control" id="exampleInputPassword1" placeholder="Tempat">
                    </div> 
                    <div class="form-group">
                        <label for="exampleInputPassword1">TANGGAL LAHIR</label>
                        <input type="date" value="<?= $DATA->tgl_lahir_siswa ?>" name="ttl" class="form-control" id="exampleInputPassword1" placeholder="Tanggal Lahir">
                    </div> 
                    <div class="form-group">
                        <label for="exampleInputPassword1">ALAMAT</label>
                        <input type="text" value="<?= $DATA->alamat_siswa ?>" value="<?= $DATA->alamat_siswa ?>" name="alamat" class="form-control" id="exampleInputPassword1" placeholder="Alamat">
                    </div> 
                    <div class="form-group">
                        <label for="exampleInputPassword1">NO. HP</label>
                        <input type="text" value="<?= $DATA->no_hp_siswa ?>" name="no_hp" class="form-control" id="exampleInputPassword1" placeholder="No. HP">
                    </div>  
                    <div class="form-group">
                        <label for="exampleInputPassword1">TAHUN AJARAN</label>
                        <select class="form-control" name="ta" id="daterange-btn" class="select2-selection__rendered">
                            <option value="">SELECT</option>
                            <?php
                            $no = 1;
                            foreach ($data_ta as $row) { ?>  
                                <option value="<?=$row->tahun_ajaran?>"> <?=$row->tahun_ajaran?> </option>
                            <?php } ?>
                        </select>
                    </div>  
                    <div class="form-group">
                    <label for="exampleInputPassword1">KELAS</label>
                    <select class="form-control" name="kelas" id="daterange-btn" class="select2-selection__rendered">
                        <option value="">SELECT</option>
                        <?php
                        $no = 1;
                        foreach ($data_kelas as $row) { ?>  
                            <option value="<?=$row->kode_kelas?>"><?=$row->kode_kelas?> </option>
                        <?php } ?>
                    </select>
                    </div>     
                    </div>   
                    <div class="box-footer">
                        <button type="submit" class="form-control btn btn-success">Update</button>
                        <br><br>
                        <a href="<?=base_url('a/s/delete-data?p='. $DATA->id_siswa)?>" class="form-control btn btn-danger">Delete</a>
                    </div> 
                </form>
            </div> 
        </div> 
    </div> 
</section>