<section class="content-header">
  <h1>
    DATA SISWA
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li> 
    <li class="active">Data Siswa</li>
  </ol>
</section>
<section class="content">   
  <?=$notif_berhasil?>
  <form action="<?=base_url()?>a/s/search-data" method="POST"> 
    <select name="ta" class="select2-selection__rendered">
      <option value="">TAHUN AJARAN</option>
      <?php
      $no = 1;
      foreach ($data_ta as $row) { ?>  
        <option value="<?=$row->tahun_ajaran?>"> <?=$row->tahun_ajaran?> </option>
      <?php } ?>
    </select>

    <select name="kelas" class="select2-selection__rendered">
      <option value="">KELAS</option>
      <?php
      $no = 1;
      foreach ($data_kelas as $row) { ?>  
        <option value="<?=$row->kode_kelas?>"><?=$row->kode_kelas?> </option>
      <?php } ?>
    </select>

    <input type="text" name="nama" placeholder="Nama Siswa">

    <button type="submit" name="btnSearch" class="btn bg-blue btn-flat margin" title="search"> <i class="fa fa-search"></i> </button>

    <a href="<?= base_url() ?>a/s/insert-data" class="btn bg-blue btn-flat margin pull-right">Add</a>    
    <a href="<?= base_url() ?>a/s/import-data" class="btn bg-blue btn-flat margin pull-right">Import</a>    
    <div class="box"> 
      <!-- /.box-header -->
      <div class="box-body table-responsive no-padding  direct-chat-messages">
        <table class="table table-hover">
          <tr> 
            <th>NO.</th>
            <th>NO. INDUK</th>
            <th>NISN</th>
            <th>NAMA</th>
            <th>KELAMIN</th>
            <th>TTL</th>
            <th>ALAMAT</th>
            <th>No. HP</th> 
            <th>KELAS</th>
            <th>TAHUN AJARAN</th>
            <th>STATUS</th>
            <th> <center>ACTION</center> </th>
          </tr>

          <?php 
              if($this->uri->segment(4)){
                $no=$this->uri->segment(4);
              }
              else{
                $no=0;
              }
              foreach ($data_siswa->result() as $row) { 
                $no++;  
                
                if($row->status_smart_card == 1){?>  
                <tr> 
                  <td><?= $no;?></td>  
                  <td><?=$row->no_induk_siswa?></td>  
                  <td><?=$row->nisn_siswa?></td>  
                  <td><?=$row->nama_siswa?></td>  
                  <td><?=$row->jenkel_siswa?></td>  
                  <td><?=$row->tempat_siswa . ", " . mediumdate_indo($row->tgl_lahir_siswa)?></td>  
                  <td><?=$row->alamat_siswa?></td>  
                  <td><?=$row->no_hp_siswa?></td>   
                  <td><?=$row->kode_kelas?></td>   
                  <td><?=$row->tahun_ajaran?></td>  
                  <td> <a href="<?= base_url('edit-status-card-siswa?id_siswa='.$row->id_siswa.'&status='.$row->status_smart_card) ?>" class="btn btn-xs btn-success" title="non-active now">REGISTERED</a> </td> 
                  <td align="center">
                    <a href="<?= base_url('a/s/update-data?p='.$row->id_siswa) ?>" class="btn btn-warning btn-xs fa fa-pencil" title="edit"></a>
                  </td> 
                </tr>
                <?php }else{ ?>
                  <tr> 
                  <td><?= $no;?></td>  
                  <td><?=$row->no_induk_siswa?></td>  
                  <td><?=$row->nisn_siswa?></td>  
                  <td><?=$row->nama_siswa?></td>  
                  <td><?=$row->jenkel_siswa?></td>  
                  <td><?=$row->tempat_siswa . ", " . mediumdate_indo($row->tgl_lahir_siswa)?></td>  
                  <td><?=$row->alamat_siswa?></td>  
                  <td><?=$row->no_hp_siswa?></td>  
                  <td><?=$row->kode_kelas?></td>   
                  <td><?=$row->tahun_ajaran?></td>  
                  <td> <a href="<?= base_url('edit-status-card-siswa?id_siswa='.$row->id_siswa.'&status='.$row->status_smart_card) ?>" class="btn btn-xs btn-danger" title="active now">UNREGISTER</a> </td> 
                  <td align="center">
                    <a href="<?= base_url('a/s/update-data?p='.$row->id_siswa) ?>" class="btn btn-warning btn-xs fa fa-pencil" title="edit"></a>
                  </td> 
                </tr>
              <?php 
                } 
              }?> 
              
        </table>
      </div> 
      <div class="box-footer clearfix">
        <ul class="pagination pagination-sm no-margin pull-left">
              <?php
                if($data_siswa->num_rows() > 0){
                  $count;
                }else{
                  $count = 0;
                }
              ?>
           <?php  
              if($data_siswa->num_rows() > 0){
                $count;
              }else{
                $count = 0;
              }
             ?> 
          <b>TOTAL SISWA</b> : <?=$count?> 
        </ul>
        <div align="right">
          <span style="color:red"> <?= $notif_limit ?> </span>
        </div>
        <?= $pagination?>  
      </div>
    </div>
  </form>
</section> 