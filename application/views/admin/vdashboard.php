<section class="content-header">
  <h1>
    <!-- Top Navigation
    <small>Example 2.0</small> -->
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li> 
  </ol>
</section> <br>
<section class="content">
  <div class='alert alert-info alert-dismissible'>
    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
    <h4 style="color:black"> Welcome!</h4> 
    <i style="color:black">
      Selamat datang dihalaman admin!
    </i>
  </div>  
  <div class="box box-default">
    <div class="box-header with-border">
      <h3 class="box-title"> <i class="fa fa-cog"></i> Monitoring Alat</h3>

    </div>
    <div class="box-body"> 
      <div class="row direct-chat-messages">
        <?php  
        foreach ($DATA as $row) {
          if($row->tgl_now == $row->tgl_old && $row->time_now < $row->time_old){ ?>
            <div class="col-md-3">
              <div class="box box-success">
                <div class="box-header" align="center">
                  <h3 class="box-title"> <?= $row->nama_alat ?> </h3><br>
                  TOKEN : <b><?= $row->kode_token ?></b> <br>
                  Online : NOW
                </div>
                <div class="box-body" align="center">
                  <a href="<?=base_url('update-timenow?token='.$row->kode_token)?>" title="refresh">
                    <span class=" btn bg-olive btn-flat margin">  
                      <i class="fa fa-refresh fa-spin"></i>
                      ONLINE 
                    </span>
                  </a>
                </div>  
              </div> 
            </div>
          <?php } else {?>
            <div class="col-md-3">
              <div class="box box-danger">
                <div class="box-header" align="center">
                  <h3 class="box-title"> <?= $row->nama_alat ?> </h3><br>
                  TOKEN :  <b><?= $row->kode_token ?></b>  <br>
                  LAST : <?=$row->time_old?>
                </div>
                <div class="box-body" align="center">
                  <a href="<?=base_url('update-timenow?token='.$row->kode_token)?>" title="refresh">
                    <span class=" btn bg-maroon btn-flat margin"> 
                      <i class="fa fa-refresh fa-spin"></i>
                      DISCONNECT 
                    </span> 
                  </a>
                </div>  
              </div> 
            </div>
          <?php } 
        }?>  
      </div>
    </div> 
  </div> 
</section>  