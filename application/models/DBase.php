<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DBase extends CI_Model {
    function get_id($table, $id)
    {
        $this->db->select_max($id);
        $query = $this->db->get($table);
        return $query;
    }

    function insert_data($table, $data)
    {
        $this->db->insert($table, $data);
    }

    function insert_data_batch($table, $data)
    {
        $this->db->insert_batch($table, $data);
    }

    function update_data($table, $where, $data)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    } 

    function get_data($table, $order="")
    {
        $this->db->order_by($order);
        return $this->db->get($table);
    } 

    function get_where_data($table, $where)
    {
        return $this->db->get_where($table,$where);
    } 

    function delete_data($table, $where)
    {
        $this->db->where($where);
        return $this->db->delete($table);
    }

    function search_data_siswa($nama="", $kelas="", $ta="", $limit="", $start="")
    {
        $this->db->select('*')
                 ->from('tb_siswa')
                 ->join('tb_kelas', 'tb_kelas.kode_kelas = tb_siswa.kode_kelas', 'inner');
        $this->db->order_by('tahun_ajaran', 'ASC')
                 ->order_by('tb_siswa.kode_kelas', 'ASC')
                 ->order_by('nama_siswa', 'ASC')
                 ->like('nama_siswa', $nama, 'both')
                 ->where('tahun_ajaran',$ta)
                 ->where('tb_siswa.kode_kelas', $kelas)
                 ->limit($limit, $start);
        return $this->db->get(); 
    }

    function count_data_siswa($kelas="", $ta="")
    {
        return $this->db->get_where('tb_siswa', ['kode_kelas' => $kelas, 'tahun_ajaran' => $ta]);
    } 

    function get_data_siswa($limit="", $start=""){   
        $this->db->order_by('status_smart_card', 'ASC')
                 ->order_by('tahun_ajaran', 'ASC')
                 ->order_by('tb_siswa.kode_kelas', 'ASC')
                 ->order_by('nama_siswa', 'ASC');
		return $this->db->get('tb_siswa',$limit, $start);		
    }

    function get_where_data_siswa($where){  
        $this->db->select('*')
                 ->from('tb_siswa')
                 ->join('tb_kelas', 'tb_kelas.kode_kelas = tb_siswa.kode_kelas', 'inner')
                 ->order_by('nama_siswa', 'ASC')
                 ->where($where);
        return $this->db->get(); 		
    }

    function get_siswa($where="")
    {
        if ($where) {
            return $this->db->select('*')
                        ->from('tb_siswa')
                        ->join('tb_ta', 'tb_ta.tahun_ajaran = tb_siswa.tahun_ajaran', 'inner')
                        ->join('tb_kelas', 'tb_kelas.kode_kelas = tb_siswa.kode_kelas', 'inner')
                        ->where($where)
                        ->order_by('tb_ta.tahun_ajaran')
                        ->order_by('nama_siswa')
                        ->get();
        }else {
            return $this->db->select('*')
                        ->from('tb_siswa')
                        ->join('tb_ta', 'tb_ta.tahun_ajaran = tb_siswa.tahun_ajaran', 'inner')
                        ->join('tb_kelas', 'tb_kelas.kode_kelas = tb_siswa.kode_kelas', 'inner')
                        ->order_by('tb_ta.tahun_ajaran')
                        ->order_by('nama_siswa')
                        ->get();
        }
    } 

    function get_report_harian_siswa($where){
        $this->db->select('*')
                 ->from('tb_absensi_siswa')
                 ->join('tb_siswa', 'tb_siswa.id_siswa = tb_absensi_siswa.id_siswa', 'right')  
                 ->join('tb_kelas', 'tb_kelas.kode_kelas = tb_kelas.kode_kelas', '') 
                 ->order_by('nama_siswa', 'ASC')
                 ->where($where);
        return $this->db->get(); 
    }

    function get_report_bulanan_siswa($where, $whereMulai, $whereAkhir){
        $this->db->select('*')
                 ->from('tb_absensi_siswa')
                 ->join('tb_siswa', 'tb_siswa.id_siswa = tb_absensi_siswa.id_siswa', 'left') 
                 ->where($where)
                 ->where($whereMulai)
                 ->where_in($whereAkhir); 
        return $this->db->get(); 
    }
    
    function search_data_pegawai($nama="", $kode="", $limit="", $start="")
    {
        $this->db->select('*') 
                 ->from('tb_pegawai')
                 ->join('tb_jabatan', 'tb_jabatan.kode_pegawai = tb_pegawai.kode_pegawai', 'inner')
                 ->order_by('status_smart_card', 'ASC')
                 ->order_by('tb_pegawai.kode_pegawai', 'ASC') 
                 ->order_by('nama_pegawai', 'ASC')
                 ->like('nama_pegawai', $nama, 'both') 
                 ->where('tb_pegawai.kode_pegawai', $kode)
                 ->limit($limit, $start);
        return $this->db->get(); 
    }

    function count_data_pegawai($kode="")
    {
        return $this->db->get_where('tb_pegawai', ['kode_pegawai' => $kode]);
    } 

    function get_data_pegawai($limit="", $start=""){  
        $this->db->select('*')
                 ->from('tb_pegawai')
                 ->join('tb_jabatan', 'tb_jabatan.kode_pegawai = tb_pegawai.kode_pegawai', 'inner')
                 ->order_by('status_smart_card', 'ASC')
                 ->order_by('tb_pegawai.kode_pegawai', 'ASC') 
                 ->order_by('nama_pegawai', 'ASC')
                 ->limit($limit, $start);
		return $this->db->get();		
    }

    function get_pegawai($where="")
    {
        if ($where) {
            return $this->db->select('*, tb_jabatan.jabatan_pegawai')
                        ->from('tb_pegawai')
                        ->join('tb_jabatan', 'tb_jabatan.kode_pegawai = tb_pegawai.kode_pegawai', 'inner')
                        ->where($where)
                        ->order_by('tb_pegawai.kode_pegawai')
                        ->order_by('nama_pegawai')
                        ->get();
        }else {
            return $this->db->select('*, tb_jabatan.jabatan_pegawai')
                        ->from('tb_pegawai')
                        ->join('tb_jabatan', 'tb_jabatan.kode_pegawai = tb_pegawai.kode_pegawai', 'inner')
                        ->order_by('tb_pegawai.kode_pegawai')
                        ->order_by('nama_pegawai')
                        ->get();
        }
    } 
    
    function get_report_harian_pegawai($kode_pegawai="", $tgl){
        $this->db->select('*')
                 ->from('tb_absensi_pegawai')
                 ->join('tb_pegawai', 'tb_pegawai.id_pegawai = tb_absensi_pegawai.id_pegawai', 'right')  
                 ->join('tb_jabatan', 'tb_jabatan.kode_pegawai = tb_pegawai.kode_pegawai', 'right')
                 ->like('tb_jabatan.kode_pegawai', $kode_pegawai, 'both') 
                 ->order_by('tb_jabatan.kode_pegawai')
                 ->order_by('nama_pegawai')
                 ->where($tgl);
        return $this->db->get(); 
    }

    function get_report_bulanan_pegawai($kode_pegawai="", $month, $year){
        $this->db->select('*')
                 ->from('tb_absensi_pegawai')
                 ->join('tb_pegawai', 'tb_pegawai.id_pegawai = tb_absensi_pegawai.id_pegawai', 'left') 
                 ->like('kode_pegawai', $kode_pegawai, 'both')
                 ->order_by('nama_pegawai', 'ASC')
                 ->where('MONTH(tb_absensi_pegawai.tanggal)', $month)
                 ->where('YEAR(tb_absensi_pegawai.tanggal)', $year);
        return $this->db->get(); 
    }
}
