-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 15, 2019 at 01:29 PM
-- Server version: 10.4.7-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_presensi`
--

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `login_attempts`
--

INSERT INTO `login_attempts` (`id`, `ip_address`, `login`, `time`) VALUES
(1, '::1', 'admin', 1568527752);

-- --------------------------------------------------------

--
-- Table structure for table `tb_absensi_pegawai`
--

CREATE TABLE `tb_absensi_pegawai` (
  `id_absensi` char(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` date NOT NULL,
  `jam_masuk` time NOT NULL,
  `jam_keluar` time NOT NULL,
  `keterlambatan` time NOT NULL,
  `status_masuk` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_keluar` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_pegawai` char(10) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_absensi_pegawai`
--

INSERT INTO `tb_absensi_pegawai` (`id_absensi`, `tanggal`, `jam_masuk`, `jam_keluar`, `keterlambatan`, `status_masuk`, `status_keluar`, `id_pegawai`) VALUES
('6AeqypDFVH', '2019-09-15', '00:00:00', '00:00:00', '00:00:00', 'A', 'A', 'fdvWZNl03'),
('8PfCZ9YVXR', '2019-09-15', '00:00:00', '00:00:00', '00:00:00', 'A', 'A', 'AHuMaSX03'),
('9wNyF40o1r', '2019-09-15', '00:00:00', '00:00:00', '00:00:00', 'H', 'H', 'DdgIrHu03'),
('B9ZIbmEoU6', '2019-09-13', '07:00:00', '15:16:16', '00:00:00', 'H', 'H', 'DdgIrHu03'),
('BZmH2Qp5ea', '2019-09-13', '00:00:00', '00:00:00', '00:00:00', 'A', 'H', 'fdvWZNl03'),
('cwXb7tGjWR', '2019-09-14', '00:00:00', '00:00:00', '00:00:00', 'A', 'H', 'fdvWZNl03'),
('kbOYgc7ayf', '2019-09-14', '00:00:00', '00:00:00', '00:00:00', 'A', 'A', 'rh1qXHZ02'),
('L7IXCDTBEw', '2019-09-13', '00:00:00', '00:00:00', '00:00:00', 'A', 'A', 'AHuMaSX03'),
('l9OrQF61TH', '2019-09-13', '00:00:00', '00:00:00', '00:00:00', 'A', 'H', 'rh1qXHZ02'),
('MC9GX2sI0t', '2019-09-14', '00:00:00', '00:00:00', '00:00:00', 'A', 'A', 'DdgIrHu03'),
('pXcY8PEGBr', '2019-09-14', '00:00:00', '00:00:00', '00:00:00', 'A', 'A', 'AHuMaSX03'),
('RF1z37lKnS', '2019-09-15', '00:00:00', '00:00:00', '00:00:00', 'A', 'A', 'rh1qXHZ02');

-- --------------------------------------------------------

--
-- Table structure for table `tb_absensi_siswa`
--

CREATE TABLE `tb_absensi_siswa` (
  `id_absensi` char(20) NOT NULL,
  `jam_masuk` time NOT NULL,
  `keterlambatan` char(10) NOT NULL,
  `tanggal` date NOT NULL,
  `ta` char(10) NOT NULL,
  `id_siswa` char(10) NOT NULL,
  `status` enum('A','H','S','I') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_absensi_siswa`
--

INSERT INTO `tb_absensi_siswa` (`id_absensi`, `jam_masuk`, `keterlambatan`, `tanggal`, `ta`, `id_siswa`, `status`) VALUES
('5NqjkET7aW', '00:00:00', '0', '2019-09-15', '2018-2019', '4ytoxze03', 'A'),
('6WSRM5FmP0', '00:00:00', '0', '2019-09-13', '2018-2019', 'opRzSkM01', 'A'),
('9lrkYb4cxf', '00:00:00', '0', '2019-09-14', '2018-2019', '4ytoxze03', 'A'),
('9wupmPCSWK', '00:00:00', '0', '2019-09-13', '2018-2019', 'afIbTnl03', 'A'),
('BNjsYWq5nb', '00:00:00', '0', '2019-09-14', '2018-2019', 'x9ZLA1l02', 'A'),
('Bwj1rUDxqk', '00:00:00', '0', '2019-09-13', '2018-2019', 'x9ZLA1l02', 'A'),
('CItMjn8pDa', '00:00:00', '0', '2019-09-15', '2018-2019', 'opRzSkM01', 'A'),
('D8yhSijN3x', '05:45:31', '0', '2019-09-15', '2018-2019', 'afIbTnl03', 'H'),
('IbLsvpFhDV', '00:00:00', '0', '2019-09-14', '2018-2019', 'P83KYkd03', 'A'),
('jNJtlHTrac', '00:00:00', '0', '2019-09-13', '2018-2019', 'P83KYkd03', 'A'),
('NZPd8GIYOx', '00:00:00', '0', '2019-09-15', '2018-2019', 'x9ZLA1l02', 'A'),
('rXpbnAxLWI', '00:00:00', '0', '2019-09-13', '2018-2019', '4ytoxze03', 'A'),
('tfNIGVHya3', '00:00:00', '0', '2019-09-14', '2018-2019', 'opRzSkM01', 'A'),
('VCIkYLUe8T', '00:00:00', '0', '2019-09-15', '2018-2019', 'P83KYkd03', 'A'),
('veF4Nuf6WQ', '00:00:00', '0', '2019-09-14', '2018-2019', 'afIbTnl03', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `tb_alat`
--

CREATE TABLE `tb_alat` (
  `kode_alat` char(10) NOT NULL,
  `nama_alat` varchar(20) NOT NULL,
  `kode_token` char(20) NOT NULL,
  `status_alat` char(1) NOT NULL,
  `tgl_now` date NOT NULL,
  `tgl_old` date NOT NULL,
  `time_now` time NOT NULL,
  `time_old` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_alat`
--

INSERT INTO `tb_alat` (`kode_alat`, `nama_alat`, `kode_token`, `status_alat`, `tgl_now`, `tgl_old`, `time_now`, `time_old`) VALUES
('123', 'Absensi-1', '5ea314104c1ae8553912', '1', '2019-09-15', '2019-09-15', '20:07:55', '20:29:22'),
('123123', 'Absensi-2', '8a190a7ff5333b140439', '0', '2019-09-13', '0000-00-00', '17:24:40', '22:34:01'),
('1234', 'Absensi-3', '6cedfa31cca1d70f6e26', '0', '2019-09-13', '0000-00-00', '04:48:34', '22:34:01');

-- --------------------------------------------------------

--
-- Table structure for table `tb_jabatan`
--

CREATE TABLE `tb_jabatan` (
  `kode_pegawai` char(10) NOT NULL,
  `jabatan_pegawai` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_jabatan`
--

INSERT INTO `tb_jabatan` (`kode_pegawai`, `jabatan_pegawai`) VALUES
('GU', 'GURU'),
('KEPSEK', 'KEPALA SEKOLAH'),
('TA', 'TATA USAHA');

-- --------------------------------------------------------

--
-- Table structure for table `tb_jadwal_pegawai`
--

CREATE TABLE `tb_jadwal_pegawai` (
  `kode_jadwal` enum('JDPGW') NOT NULL,
  `time_in` time NOT NULL,
  `time_in_` time NOT NULL,
  `time_out` time NOT NULL,
  `time_out_` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_jadwal_pegawai`
--

INSERT INTO `tb_jadwal_pegawai` (`kode_jadwal`, `time_in`, `time_in_`, `time_out`, `time_out_`) VALUES
('JDPGW', '07:00:00', '07:30:00', '15:00:00', '15:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `tb_jadwal_siswa`
--

CREATE TABLE `tb_jadwal_siswa` (
  `kode_jadwal` enum('PAGI','SIANG') NOT NULL,
  `time_in` time NOT NULL,
  `time_in_` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_jadwal_siswa`
--

INSERT INTO `tb_jadwal_siswa` (`kode_jadwal`, `time_in`, `time_in_`) VALUES
('PAGI', '05:00:00', '07:30:00'),
('SIANG', '12:00:00', '15:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kelas`
--

CREATE TABLE `tb_kelas` (
  `kode_kelas` varchar(10) NOT NULL,
  `nama_jurusan` varchar(50) NOT NULL,
  `kode_jadwal` char(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kelas`
--

INSERT INTO `tb_kelas` (`kode_kelas`, `nama_jurusan`, `kode_jadwal`) VALUES
('11-TITL-1', 'Teknik Instalasi Tenaga Listrik', 'PAGI'),
('12-TKJ-1', 'Teknik Komputer & Jaringan', 'SIANG'),
('12-TKR-1', 'Teknik Kendaraan Ringan', 'PAGI');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pegawai`
--

CREATE TABLE `tb_pegawai` (
  `id_pegawai` char(10) NOT NULL,
  `no_induk_pegawai` char(20) NOT NULL,
  `nama_pegawai` varchar(50) NOT NULL,
  `jenkel_pegawai` varchar(10) NOT NULL,
  `email_pegawai` varchar(50) NOT NULL,
  `kode_pegawai` char(10) NOT NULL,
  `status_smart_card` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pegawai`
--

INSERT INTO `tb_pegawai` (`id_pegawai`, `no_induk_pegawai`, `nama_pegawai`, `jenkel_pegawai`, `email_pegawai`, `kode_pegawai`, `status_smart_card`) VALUES
('AHuMaSX03', '213456774321', 'Ichwan Dwi Prastyo', 'Laki-Laki', 'ichwandpras@gmail.com', 'GU', '1'),
('DdgIrHu03', '1236789098811', 'Mohammad Eko Hardiyanto', 'Laki-Laki', 'moh.eko.hardiyanto1998@gmail.com', 'KEPSEK', '1'),
('fdvWZNl03', '123456789087', 'Jeffry Suyanto', 'Laki-Laki', 'jeff@gmail.com', 'GU', '1'),
('rh1qXHZ02', '1234567954643', 'Andri Eka', 'Laki-Laki', 'andri@gmail.com', 'TA', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tb_siswa`
--

CREATE TABLE `tb_siswa` (
  `id_siswa` char(10) NOT NULL,
  `no_induk_siswa` char(20) NOT NULL,
  `nisn_siswa` char(20) NOT NULL,
  `nama_siswa` varchar(30) NOT NULL,
  `jenkel_siswa` varchar(10) NOT NULL,
  `tempat_siswa` varchar(10) NOT NULL,
  `tgl_lahir_siswa` date NOT NULL,
  `alamat_siswa` varchar(50) NOT NULL,
  `no_hp_siswa` char(13) NOT NULL,
  `tahun_ajaran` varchar(10) NOT NULL,
  `kode_kelas` varchar(10) NOT NULL,
  `status_smart_card` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_siswa`
--

INSERT INTO `tb_siswa` (`id_siswa`, `no_induk_siswa`, `nisn_siswa`, `nama_siswa`, `jenkel_siswa`, `tempat_siswa`, `tgl_lahir_siswa`, `alamat_siswa`, `no_hp_siswa`, `tahun_ajaran`, `kode_kelas`, `status_smart_card`) VALUES
('4ytoxze03', '12345678999', '34552223443', 'Amshar Salam', 'L', 'Gresik', '2019-08-20', 'wakatobi eh eh', '0987777772211', '2018-2019', '12-TKJ-1', '1'),
('afIbTnl03', '1234567654322454', '2345345434434324', 'Zachfiandhika G. M.', 'L', 'Kalimantan', '2019-08-28', 'kalimantan timur', '0812323213213', '2018-2019', '11-TITL-1', '1'),
('opRzSkM01', '123409876111', '123456799711', 'Mohammad Eko Hardiyanto', 'L', 'Gresik', '2019-08-22', 'Banyu Urip Lor 5/72A', '21321323213', '2018-2019', '12-TKJ-1', '1'),
('P83KYkd03', '12345678909889', '2134566543123', 'Andhika Prasetyo Buditama', 'L', 'Surabaya', '2019-08-08', 'nginden 2 no. 10', '0821323213213', '2018-2019', '12-TKJ-1', '1'),
('x9ZLA1l02', '12345323422', '23145313124', 'Pradipta Bagus Suryanda', 'L', 'Pasuruan', '2019-08-06', 'Perum Bukit Alam Blok K-06', '081112331333', '2018-2019', '12-TKJ-1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tb_ta`
--

CREATE TABLE `tb_ta` (
  `tahun_ajaran` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_ta`
--

INSERT INTO `tb_ta` (`tahun_ajaran`) VALUES
('2018-2019'),
('2019-2020'),
('2020-2021');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(11) NOT NULL,
  `nama_user` varchar(50) NOT NULL,
  `username_user` varchar(20) NOT NULL,
  `password_user` varchar(20) NOT NULL,
  `key_num_user` varchar(20) NOT NULL,
  `status_user` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(254) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '', NULL, NULL, NULL, 1268889823, 1568527758, 1, 'Admin', 'istrator', 'ADMIN', '082233418595');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(3, 1, 1),
(4, 1, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_absensi_pegawai`
--
ALTER TABLE `tb_absensi_pegawai`
  ADD PRIMARY KEY (`id_absensi`),
  ADD KEY `id_pegawai` (`id_pegawai`);

--
-- Indexes for table `tb_absensi_siswa`
--
ALTER TABLE `tb_absensi_siswa`
  ADD PRIMARY KEY (`id_absensi`),
  ADD KEY `id_siswa` (`id_siswa`);

--
-- Indexes for table `tb_alat`
--
ALTER TABLE `tb_alat`
  ADD PRIMARY KEY (`kode_alat`);

--
-- Indexes for table `tb_jabatan`
--
ALTER TABLE `tb_jabatan`
  ADD PRIMARY KEY (`kode_pegawai`);

--
-- Indexes for table `tb_jadwal_siswa`
--
ALTER TABLE `tb_jadwal_siswa`
  ADD PRIMARY KEY (`kode_jadwal`);

--
-- Indexes for table `tb_kelas`
--
ALTER TABLE `tb_kelas`
  ADD PRIMARY KEY (`kode_kelas`);

--
-- Indexes for table `tb_pegawai`
--
ALTER TABLE `tb_pegawai`
  ADD PRIMARY KEY (`id_pegawai`),
  ADD KEY `kode_pegawai` (`kode_pegawai`);

--
-- Indexes for table `tb_siswa`
--
ALTER TABLE `tb_siswa`
  ADD PRIMARY KEY (`id_siswa`),
  ADD KEY `kode_kelas` (`kode_kelas`),
  ADD KEY `id_ta` (`tahun_ajaran`);

--
-- Indexes for table `tb_ta`
--
ALTER TABLE `tb_ta`
  ADD PRIMARY KEY (`tahun_ajaran`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
